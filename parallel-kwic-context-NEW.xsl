<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
    <xsl:include href="settings/corpus-specific.xsl"/>
<xsl:strip-space elements="*"/> 
    <xsl:output method="html"/>
    <xsl:template match="/">
       <html>
            <head>
				<title>Context</title>
				<link rel="stylesheet" href="bootstrap.css" type="text/css" />
				<style>body { padding-top: 66px; max-width: 100%; }</style>

                <link rel="stylesheet" href="css/form.css" type="text/css" />
                <link rel="stylesheet" href="css/feedback.css" type="text/css" />
                <link rel="stylesheet" href="css/fontello.css" type="text/css" />
                <link rel="stylesheet" href="css/jquery-ui-1.8.17.custom.css" type="text/css" />
				<script src="https://maps.googleapis.com/maps/api/js"></script>
				<script src="maps.js"></script>
				<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function() {
             /*       $('td.text').click(function() {
                    var myCol = $(this).index();
                    var $tr = $(this).closest('tr');
                    var myRow = $tr.index();
                    var table = $(this).closest('table');
                    var lang = $(table).find("tr").eq(1).find("td").eq(myCol).html();
                    var primlang = $(table).find("tr").eq(1).find("td").eq(0).html();
                    var token_nr = $(table).find("tr").eq(myRow).find("td").eq(0).find("font").eq(0).html();
                    var corpus = $(table).find("i").eq(0).html();
                    var url = 'context_xml.php?corpus=' + corpus + '&amp;lang=' + lang + '&amp;primlang=' + primlang + '&amp;token_nr=' + token_nr;
                    var windowName = corpus + "_" + primlang + ":" + token_nr + " in " + corpus + "_" + lang;
                    var windowSize = "width=300,height=400,scrollbars=yes";
                    window.open(url, windowName, windowSize);
                    });*/
                    });
					
					 
					 function post(path, params, method) {
							method = method || "post"; // Set method to post by default if not specified.
							console.log (params.length);
							// The rest of this code assumes you are not using a library.
							// It can be made less wordy if you use one.
							var form = document.createElement("form");
							form.setAttribute("method", method);
							form.setAttribute("action", path);

							for(var key in params) {
								if(params.hasOwnProperty(key)) {
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", key);
									hiddenField.setAttribute("value", params[key]);

									form.appendChild(hiddenField);
								 }
							}
							document.body.appendChild(form);
							form.submit();
						}

						  var allChecked = false;
						  var checkAll = function () {
						    console.log ('check all');
							$('input.check').attr('checked',!allChecked)
							allChecked = !allChecked;
							if (allChecked === true)
								$('#check_button').attr('value', 'uncheck all');
							else
								$('#check_button').attr('value', 'check all');
						  };
						  var exportCsv = function () {
							var selected = [];
							var langs = [];
							$('tr').first().next().find('td').each (function (){
								langs.push ($(this).text());
							});
							selected.push (langs);
							var trs = $('input.check:checked').closest ('tr');
							$(trs).each (function (){
								tds = $(this).find ('td:not(:first-child)');
								console.log (tds.length);
								tdtexts = [];
								$(tds).each (function (){
									tdtexts.push ($(this).text ().replace(/^\s+|\s+$/g, ''))
								});
								selected.push (tdtexts);
							});
							var jselected = JSON.stringify (selected);
							post ('export_csv.php', {selected: jselected});
						  }					
                </script>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>
                <!--<script type="text/javascript" src="js/feedback.js"></script>-->

						<script type="text/javascript">
				function play_sound(s) {
						document.getElementById(s).play();
				}
		</script>
            </head>
            <body>
	  <div role="navigation" class="navbar navbar-fixed-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="." class="navbar-brand"><xsl:value-of select="$GRAPHIC-HEADER" /></a>
        </div>
			<div style="text-align: right; padding-top: 24px; padding-right: 40px;">
			<a href="." style="color: black; text-decoration: none;">back to the search page >></a>
			</div>
        </div>
      </div>
				<div class="container" style="margin-top: 40px;">
				<main role="main">
				<div class="row marketing">
                <xsl:apply-templates/>
				</div>
				</main>
				</div>
      <footer class="footer">
				<!--<p align="center" style="color: white;"><a href="https://ilcl.hse.ru/en/" style="font-size:14px; color: white; text-decoration: none;">Linguistic Convergence Laboratory</a>, NRU HSE  </p>-->
      </footer>
            </body>
        </html>
    </xsl:template>


    <xsl:template match="RESULTS/text()">
        <xsl:element name="b">
            <xsl:value-of select="'('"/>
            <xsl:value-of select="."/>
            <xsl:value-of select="' hit'"/>
            <xsl:if test=".&gt;1">
                <xsl:value-of select="'s'"/>
            </xsl:if>
            <xsl:value-of select="'.)'"/>
					
        </xsl:element>
        <xsl:element name="p"/>
    </xsl:template>
		
    <xsl:template match="CONCORDANCE">
        <table class="gradient shadow" style="font-size: small; border-spacing: 1; background-color: gray; width: 100%; border-color: grey;"> 
            <tr style="background-color: white">
                <td colspan="3">
                  	<xsl:copy-of select="$CORPUSHITSLABEL"/>
              </td>

            </tr>
					<!-- Überschrift -->
            <xsl:element name="tr">
               <!-- <xsl:element name="td">-->
                <xsl:value-of select="/RESULTS/@primlang"/>
                <!-- </xsl:element> -->
                <xsl:variable name="fpos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[2]/following-sibling::ALIGN)"/>
                <xsl:for-each select="LINE[1]/following-sibling::ALIGN[position() &lt;= $fpos]">
                   <!-- <xsl:element name="td"> -->
                    <xsl:value-of select="substring-after(@name, '_')"/>
                    <!-- </xsl:element> -->
                </xsl:for-each>
            </xsl:element>
					
            <xsl:variable name="npos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[1]/following-sibling::LINE/following-sibling::ALIGN)"/>
            <xsl:variable name="numberColumns" select="count (LINE[1]/following-sibling::ALIGN[position() &lt;= $npos])"/>


					
            <xsl:for-each select="LINE">
                <xsl:element name="tr">
                    <xsl:variable name="pos" select="position()" />
                    <xsl:attribute name="id">
                        <xsl:value-of select="$pos"/>
                    </xsl:attribute>
                    <xsl:if test="(position() mod 2)=1">
                        <xsl:attribute name="style">color: black; background-color: #FFFFFF;</xsl:attribute>
                    </xsl:if>
                    <xsl:if test="(position() mod 2)=0">
                        <xsl:attribute name="style">color: black; background-color: #FFDDAA;</xsl:attribute>
                    </xsl:if>
					
					
                    <!-- <xsl:element name="td">-->
                    <xsl:apply-templates/>
                    <!-- </xsl:element> -->
                    <xsl:for-each select="following-sibling::ALIGN[position() &lt;= $npos]">
                        <xsl:element name="td">
                            <xsl:attribute name="width">
                                <xsl:value-of select="(100 div ($numberColumns+1))"/>
                                <xsl:value-of select="'%'"/>
                            </xsl:attribute>
					<!-- 					<xsl:value-of select="position()"/>  -->
                            <xsl:attribute name="title"></xsl:attribute>
                            <xsl:apply-templates/>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:element>
            </xsl:for-each>
        </table>					
    </xsl:template>

    <xsl:template match="file|from|to|text()"/>


    <xsl:template match="MATCHNUM">
        <xsl:element name="td">
            <xsl:attribute name="style">width:100px</xsl:attribute>
            <font>
                <xsl:value-of select="text()"/>
                <xsl:element name="a">
                    <xsl:attribute name="class">icon-doc-text-inv</xsl:attribute>
                    <xsl:attribute name="title">zapisz link</xsl:attribute>
                    <xsl:attribute name="style">color:blue;</xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('./context_xml_byTimeID.php?&amp;from=', following-sibling::STRUCS/from, '&amp;file=', following-sibling::STRUCS/file, '&amp;to=', following-sibling::STRUCS/to)"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:value-of select="'_blank'"/>
                    </xsl:attribute>
                </xsl:element>
                                <xsl:element name="a">
                    <xsl:attribute name="class">icon-doc-text-inv</xsl:attribute>
                    <xsl:attribute name="title">kopiuj CSV</xsl:attribute>
                    <xsl:attribute name="style">color:green;</xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('./context_xml_byTimeID_csv.php?&amp;from=', following-sibling::STRUCS/from, '&amp;file=', following-sibling::STRUCS/file, '&amp;to=', following-sibling::STRUCS/to)"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:value-of select="'CSV'"/>
                    </xsl:attribute>
                </xsl:element>

            </font>
        </xsl:element>
    </xsl:template>

    <xsl:template match="MATCH">
        <font color="red" >
            <b>
                <xsl:apply-templates/>
            </b>
        </font>
    </xsl:template>

    <xsl:template match="STRUCS">
        <xsl:element name="td">
            <xsl:attribute name="style">width:100px</xsl:attribute>
            <xsl:value-of select="concat('Speaker: ', spkr, ' ')"/>
               
            <xsl:element name="a">
                <xsl:attribute name="class">icon-headphones</xsl:attribute>
                <xsl:attribute name="title">link do nagrania</xsl:attribute>
                <xsl:attribute name="href">
                    <xsl:value-of select="concat('./OUT/','')"/>
                    <xsl:value-of select="concat(file, '-',from,'-', to, '.wav')"/>
                </xsl:attribute>
            </xsl:element>
        
            <xsl:element name="br">
            </xsl:element>
            <xsl:element name="audio">
                <xsl:attribute name="controls"/>

                <xsl:attribute name="preload">none</xsl:attribute>
                <xsl:element name="source">
                    <xsl:attribute name="src">
                        <xsl:value-of select="concat('./OUT/','')"/>
                        <xsl:value-of select="concat(file, '-',from,'-', to, '.wav')"/>
                    </xsl:attribute>
                    <xsl:attribute name="type">audio/wav</xsl:attribute>
                </xsl:element>
            </xsl:element>

          <xsl:element name="br"/>

          </xsl:element>
	
    </xsl:template>
    
    
    
    
    <!--  create div over content -->
    <xsl:template match="spkr">
    <xsl:if test="count(preceding::spkr) =1  or preceding::spkr[1] != . ">
    <xsl:if test="count(preceding::spkr) &gt;1  ">
    <br/>
    </xsl:if>
    <i><xsl:value-of select="concat(., ': ')"/></i>
    </xsl:if>
    </xsl:template>


 


    <xsl:template match="CONTENT">
        <xsl:element name="td">       
            <xsl:variable name="from" select="../STRUCS/fromId" />
            <xsl:variable name="to" select="../STRUCS/toId" />
            <xsl:variable name="file" select="../STRUCS/file" />
            <xsl:variable name="usr" select="../STRUCS/usr" />
            <xsl:variable name="sts" select="../STRUCS/sts" />
            <xsl:variable name="audiofile" select="concat('./OUT/',../STRUCS/file, '-',../STRUCS/from,'-', ../STRUCS/to, '.wav')"/>
            <i id="icons" class="icon-edit editButton" title="Edit Entry" onclick="openEditDialog($(this),'{$from}','{$to}','{$file}','{$usr}', '{$audiofile}')"/>
            <xsl:choose>
                <xsl:when test="$sts='new'">
                    <i id="icons" class="icon-alert alertButton" title="Last change was not yet approved!"/>
                </xsl:when>
                <xsl:when test="$sts='ok'">
                    <i id="icons" class="icon-ok okButton" title="Change was approved!"/>
                </xsl:when>
            </xsl:choose>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>    
    
    <xsl:template match="TOKEN">
        <xsl:element name="font">
            <xsl:attribute name="title">
                <xsl:value-of select="ANNOT"/>
            </xsl:attribute>
            <xsl:value-of select="text()"/>
<xsl:if test="not ( starts-with(following-sibling::TOKEN[1], '.') or starts-with(following-sibling::TOKEN[1], ',') or starts-with(following-sibling::TOKEN[1], ':') or starts-with(following-sibling::TOKEN[1], '?') or starts-with(following-sibling::TOKEN[1], '!'))"> 
<xsl:value-of select="' '"/></xsl:if>        </xsl:element>


   <xsl:if test="./preceding-sibling::spkr[1]/text() != 'Eksplorator' and following-sibling::*[1][self::from|from] or position() = last()">
            <xsl:element name="a">
                    <xsl:attribute name="href">
			<xsl:text>javascript:play_sound(&apos;</xsl:text>
                        <xsl:value-of select="concat(preceding::file[1], '-',preceding::from[1],'-', preceding::to[1])"/>
			<xsl:text>&apos;);</xsl:text>
                        </xsl:attribute><xsl:value-of select="' ♩ '"/>
            </xsl:element>


            <xsl:element name="audio">
                <xsl:attribute name="preload">auto</xsl:attribute>
                    <xsl:attribute name="id">
                        <xsl:value-of select="concat(preceding::file[1], '-',preceding::from[1],'-', preceding::to[1])"/>
                    </xsl:attribute>
                    <xsl:attribute name="src">
                        <xsl:value-of select="concat('./OUT/','')"/>
                        <xsl:value-of select="concat(preceding::file[1], '-',preceding::from[1],'-', preceding::to[1], '.wav')"/>
                    </xsl:attribute>
		    <xsl:attribute name="type">audio/wav</xsl:attribute>
		    <xsl:element name="source">
                    <xsl:attribute name="src">
                        <xsl:value-of select="concat('./OUT/','')"/>
                        <xsl:value-of select="concat(preceding::file[1], '-',preceding::from[1],'-', preceding::to[1], '.wav')"/>
                    </xsl:attribute>
                    <xsl:attribute name="type">audio/wav</xsl:attribute>
                </xsl:element>
            </xsl:element>



    </xsl:if>






    </xsl:template>

    <xsl:template match="s">
        <xsl:element name="sup">
            <xsl:value-of select="@id"/>
        </xsl:element>
    </xsl:template>


    <xsl:template name="translTime">	
	<xsl:param name="time" />
	<xsl:if test ="(floor (($time div 1000) div 3600)) &gt; 0"><xsl:value-of select="concat(floor (($time div 1000) div 3600),':')"/></xsl:if>
	<xsl:if test ="(floor (($time div 1000) mod 3600 div 60)) &gt; -1"><xsl:value-of select="concat(floor (($time div 1000) mod 3600 div 60),':')"/></xsl:if>
	<xsl:if test ="(floor (($time div 1000) mod 60)) &gt; -1"><xsl:value-of select="concat(floor (($time div 1000) mod 60),'.')"/></xsl:if>
	<xsl:if test ="(floor (($time mod 1000))) &gt; -1"><xsl:value-of select="concat(floor (($time mod 1000)),'')"/></xsl:if>
    </xsl:template>



</xsl:stylesheet>
