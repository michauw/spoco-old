
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

        <title>SpoCo Corpus</title>

    <link href="bootstrap.css" rel="stylesheet">
	<style>body { padding-top: 66px; max-width: 100%; }</style>

    <link href="css/narrow-jumbotron.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/nav.css">
    <link rel="stylesheet/less" type="text/css" href="css/styl.less">

	    <script src="vendor/less.min.js"></script>
        <script src="vendor/angular.min.js"></script>
        <script src="vendor/angular-animate.min.js"></script>
		<script src="vendor/ui-bootstrap-tpls-0.12.1.min.js"></script>
		<script src="dist/js/angular-gettext.js"></script>
		<script src="dist/js/ngDialog.min.js"></script>
		<script src="dist/js/angular-virtual-keyboard.js"></script>
		<script type="text/javascript" src="dist/js/lodash.min.js"></script>
		<script src='dist/js/angular-simple-logger/index.js'></script>
		<script src='dist/js/angular-google-maps.js'></script>
		<script src="https://maps.googleapis.com/maps/api/js"></script>
		<script type="text/javascript" src="dist/js/angularjs-dropdown-multiselect.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>
        <!--<script type="text/javascript" src="js/initDialogs.js"></script>-->
        <script type="text/javascript" src="js/jsfunctions.js"></script>
		<script type="text/javascript" src="js/translations.js"></script>
		<!--<script type="text/javascript" src="js/keyboard.js" charset="UTF-8"></script>
        <script type="text/javascript" src="js/initDialogs.js"></script>-->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- Latest compiled and minified CSS -->

		<!-- jQuery library -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>

		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>		
		
  </head>

  <body ng-app="corpus" ng-controller="main">
      <div role="navigation" class="navbar navbar-fixed-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a translate href="." class="navbar-brand">{{title}}</a>
        </div>
			<div style="text-align: right; padding-top: 24px;">
				[
				<span translate class="languageSwitch" ng-click="switchLanguage('en')">eng</span>
				 | 
				<span translate class="languageSwitch" ng-click="switchLanguage('ru_RU')">rus</span>
				 |
				<span translate class="languageSwitch" ng-click="switchLanguage('de_DE')">ger</span>
				 |
				<span translate class="languageSwitch" ng-click="switchLanguage('pl_PL')">pol</span>				
				]
			</div>
        </div>
    </div>
	
	
	<div id="body" style="margin-top: 40px;">
    <div class="container">
        <div class="row marketing">
          <div class="col-lg-3">

		<nav class="navbar navbar-default" role="navigation" style="padding-left; 30%; border-radius: 0px 5px 5px 0px; border-color: grey;">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul style="list-style-type: none; padding-left: 0px;">
					<li translate class="bigger-font round-right as-link" style="padding: 6px; margin-left: 6px; margin-top: 6px;" ng-click="navigateTo ('corpus')">Search</li>
					<li translate class="bigger-font round-right as-link" style="padding: 6px; margin-left: 6px;" ng-click="navigateTo ('instruction')">Instruction</li>
					<li translate class="bigger-font round-right as-link" style="padding: 6px; margin-left: 6px;" ng-click="navigateTo ('statistics')">Corpus statistics</li>
					<li role="presentation" class="divider"></li>
					<li translate class="bigger-font round-right as-link" style="padding: 6px; margin-left: 6px;" ng-click="navigateTo ('project')">About the project</li>
					<li translate class="bigger-font round-right as-link" style="padding: 6px; margin-left: 6px;" ng-click="navigateTo ('team')">Team</li>
					<li translate class="bigger-font round-right as-link" style="padding: 6px; margin-left: 6px;" ng-click="navigateTo ('publications')">Publications</li> 
					<li translate class="bigger-font round-right as-link" style="padding: 6px; margin-left: 6px;" ng-click="navigateTo ('contact')">Contact</li> 
				</ul>

		  </div>
		</nav>
          </div>

          <div class="col-lg-9">
		 
		  <div class="col-lg-12 animate-switch-container" ng-switch="currentView" style="position: left">
				<div ng-switch-when="corpus" style="position: left" class="col-lg-12">
					<form action="results.php" method="POST" target="_blank">
						<div class="row">
								<language-query class="animate-repeat"></language-query>
						</div>
						<div class="row">
							<div class="col-lg-10 main-content">
									<input type="submit" id="main" name="btn[xml]" value="{{'Search'|translate}}"  class="search-button-1"/>
									<input type="submit" id="main" name="btn[xmlfile]" value="{{'Export XML'|translate}}" class="search-button-1"/>
							</div>
						</div>
					</form>
				</div>
				
				<div ng-switch-when="project">
					<info-project></info-project>
				</div>
				<div ng-switch-when="team">
					<info-team></info-team>
				</div>
				<div ng-switch-when="publications">
					<info-publications/>
				</div>
				<div ng-switch-when="statistics">
					<info-statistics/>
				</div>
				<div ng-switch-when="instruction">
					<info-instruction/>
				</div>
				<div ng-switch-when="contact">
					<info-contact/>
				</div>
			</div>
        
				
          </div>
        </div>
		<script>
			$(document).on('click', '.dropdown-menu', function (e) {
				e.stopPropagation();
			});
		</script>			
        <script src="jsapp/corpus.js"></script>
        <script src="jsapp/languageQuery/languageQuery.js"></script>
        <script src="jsapp/languageQuery/queryRow/queryRow.js"></script>
		<script src="jsapp/languageQuery/metadata/metadata.js"></script>
		<script src="jsapp/info/info.js"></script>
      </main>

      <!-- <footer class="footer">
				<p translate align="center" style="color: white;"><a href="https://ilcl.hse.ru/en/" style="color: white; text-decoration: none;">Linguistic Convergence Laboratory</a>, NRU HSE</p>
      </footer> -->
    </div> <!-- /container -->
  </body>
</html>
