﻿<?php

  header('Content-type: text/html; charset=utf-8');
  include('settings/init.php');
?>
<h3>Basic stats</h3>
<?php
  echo('Number of tokens (only informants): ');
 system("cqpcl -r ".$REGISTRY.' "'.$CORPUSNAME.'; A=[]; size A;"');
  echo('<br/>
');
  echo('Number of tokens (including interviewers and notes): ');
 system("cqpcl -r ".$REGISTRY.' "'.$CORPUSNAME.'FULL; A=[]; size A;"');
  echo('<br/>
');
?>
<h3>Tokens per speaker (only informants)</h3>
<table>
<?php
 system("cqpcl -r ".$REGISTRY.' "'.$CORPUSNAME.'; set PrintMode sgml; A=[]; group A match utterance_spkr"');
?>
</table>
<h3>Tokens per speaker (including interviewers and notes):</h3>
<table>
<?php
 system("cqpcl -r ".$REGISTRY.' "'.$CORPUSNAME.'FULL; set PrintMode sgml; A=[]; group A match utterance_spkr"');
?>
</table>

