var corpus = angular.module('corpus', ['ui.bootstrap', 'gettext', 'ngDialog', 'angular-virtual-keyboard', 'angularjs-dropdown-multiselect', 'uiGmapgoogle-maps', 'ngAnimate']);


corpus.run(function (gettextCatalog) {
    gettextCatalog.setCurrentLanguage('en');
	gettextCatalog.debug = false;
});

corpus.config(['VKI_CONFIG', function(VKI_CONFIG) {
			VKI_CONFIG.layout['Rusyn'] = {
				'name': "Rusyn", 'keys': [
				  [["\u00b4", "~"], ["1", "!"], ["2", '"'], ["3", "\u2116"], ["4", ";"], ["5", "%"], ["6", ":"], ["7", "?"], ["8", "*"], ["9", "("], ["0", ")"], ["-", "_"], ["=", "+"], ["Bksp", "Bksp"]],
				  [["Tab", "Tab"], ["\u0439", "\u0419"], ["\u0446", "\u0426"], ["\u0443", "\u0423"], ["\u043A", "\u041A"], ["\u0435", "\u0415"], ["\u043D", "\u041D"], ["\u0433", "\u0413"], ["\u0448", "\u0428"], ["\u0449", "\u0429"], ["\u0437", "\u0417"], ["\u0445", "\u0425"], ["\u0457", "\u0407"], ['\u044A', '\u042A'], ["\u0491", "\u0490"]],
				  [["Caps", "Caps"], ["\u0444", "\u0424"], ["\u0456", "\u0406"], ["\u0432", "\u0412"], ["\u0430", "\u0410"], ["\u043F", "\u041F"], ["\u0440", "\u0420"], ["\u043E", "\u041E"], ["\u043B", "\u041B"], ["\u0434", "\u0414"], ["\u0436", "\u0416"], ["\u0454", "\u0404"], ["Enter", "Enter"]],
				  [["Shift", "Shift"], ["\u044F", "\u042F"], ["\u0447", "\u0427"], ["\u0441", "\u0421"], ["\u043C", "\u041C"], ["\u0438", "\u0418"], ["\u0442", "\u0422"], ["\u044C", "\u042C"], ["\u0431", "\u0411"], ["\u044E", "\u042E"], ['\u044b', '\u042b'], ['\u0451', '\u0401'], ['\u045e', '\u040e'], [".", ","], ["Shift", "Shift"]],
				  [[" ", " "]]
				], 'lang': ["uk"] };

			VKI_CONFIG.layout['Rusyn QWERTY'] = {
				'name': 'Rusyn QWERTY', 'keys': [
				  [["\u00b4", "~", "\u00b4", "~"], ["1", "!", '1', '!'], ["2", '"', '2', '@'], ["3", "\u2116", '3', '#'], ["4", ";", '4', '$'], ["5", "%", "5", "%"], ["6", ":", '6', '^'], ["7", "?", '7', '&'], ["8", "*", '8', '*'], ["9", "(", "9", "("], ["0", ")", "0", ")"], ["-", "_", "-", "_"], ["=", "+", '=', '+'], ["Bksp", "Bksp"]],
				  [["  Tab  ", "Tab"], ['\u044b', '\u042b', 'q', 'Q'], ["\u0448", "\u0428", 'w', 'W'], ["\u0435", "\u0415", 'e', 'E'], ["\u0440", "\u0420", 'r', 'R'], ["\u0442", "\u0422", 't', 'T'], ['\u045e', '\u040e', 'y', 'Y'], ["\u0443", "\u0423", 'u', 'U'], ["\u0456", "\u0406", 'i', 'I'], ["\u043E", "\u041E", 'o', 'O'], ["\u043F", "\u041F", 'p', 'P'], ["\u0457", "\u0407", '[', '{'], ["\u0491", "\u0490", ']', '}'], ['\u0451', '\u0401'], ['\u044A', '\u042A']],
				  [["Caps", "Caps"], ["\u0430", "\u0410", 'a', 'A'], ["\u0441", "\u0421", 's', 'S'], ["\u0434", "\u0414", 'd', 'D'], ["\u0444", "\u0424", 'f', 'F'], ["\u0433", "\u0413", 'g', 'G'], ["\u0447", "\u0427", 'h', 'H'], ["\u0439", "\u0419", 'j', 'J'], ["\u043A", "\u041A", 'k', 'K'], ["\u043B", "\u041B", 'l', 'L'], ["\u0454", "\u0404", ';', ':'], ["\u0449", "\u0429", "'", '"'], ["\u0436", "\u0416", '', ''], ["Enter", "Enter", '', '']],
				  [["Shift", "Shift"], ["\u044F", "\u042F", '\\', '|'], ["\u0437", "\u0417", 'z', 'Z'], ["\u0445", "\u0425", 'x', 'X'], ["\u0446", "\u0426", 'c', 'C'], ["\u0432", "\u0412", 'v', 'V'], ["\u0431", "\u0411", 'b', 'B'], ["\u043D", "\u041D", 'n', 'N'], ["\u043C", "\u041C", 'm', 'M'], ["\u044C", "\u042C", ",", '<'], ["\u044E", "\u042E", '.', '>'], ["\u0438", "\u0418", '', ''], [".", ">", '/', '?'], ["Shift", "Shift", '', '']],
				  [["Alt", "Alt"], [" ", " "], ["AltLock", "AltLk"]]
				], 'lang': ["uk"] };
}]);

corpus.controller('main', ['$scope','queryKeeper', 'gettextCatalog', 'ngDialog', 'loadMetaData', function($scope, queryKeeper, gettextCatalog, ngDialog, loadMetaData) {
	
	var visible = false;
	var views = ['corpus', 'project', 'team', 'publications', 'instruction', 'contact']
	
	$scope.title = 'SpoCo Corpus';
	
	loadMetaData.getMetaFile ('settings/corpus-specific.xsl').then (function (response) {
		var reader = new FileReader ();
		//var text = reader.readAsText (response);
		var match = /<!-- title -->(.*)<!-- title -->/.exec (response);
		if (match !== null)
			$scope.title = match[1];
		console.log ('title:', match);
	});
	
	$scope.currentView = views[0];
	$scope.switchLanguage = function(lang) {
		gettextCatalog.setCurrentLanguage(lang);
	}
	$scope.changeVisible = function () {
		visible = !visible;
	}
	$scope.isVisible = function () {
		return visible;
	}
	
	$scope.showInfo = function () {
		//$rootScope.theme = 'ngdialog-theme-plain custom-width';
		ngDialog.open({
						template: 'jsapp/info.html', 
						className: 'ngdialog-theme-plain custom-width'
					})
	}
	
	$scope.navigateTo = function (dest) {
		$scope.currentView = dest;
	}

}]);

corpus.factory('queryKeeper', ['$rootScope', 'loadLanguages', 'loadMetaData', function($rootScope, loadLanguages, loadMetaData) {

    var queryRows = [];
	var metaFields = [];
	var metaValues = [];
	var cqpChanged = false;
	var filterGuardian = []
	var limit = [];
	var varietyAdded = 0;
	var metaValuesLoaded = false;
	
	if (!metaValuesLoaded) {
		loadMetaData.getMetaFile('settings/meta_data.json').then(function(response) {
			metaValues = angular.fromJson(response);
			metaValuesLoaded = true;
			});	
	}
	
	loadMetaData.getMetaFile('settings/meta.json').then(function(response) {
		metaFields = angular.fromJson(response);
		for (var i = 0; i < metaFields.length; ++i)
		{
			metaFields[i].negate = false;
			filterGuardian.push (1);
		}
	});
	
	var queryNegation = '';
	var match = function (value, possibleValues, key) {
			//console.log ('Match (value, possibleValues, key):', value, possibleValues, key);
			var found = false;
			needle = value[key];
			for (var i = 0; i < possibleValues.length; ++i)
				if (needle == possibleValues[i])
				{
					found = true;
					break;		
				}
			if (found) {
				return true;
			}
			return false;
		};
		
	var compare = function (a,b) {
				if (a.label > b.label)
					return 1;
				if (a.label < b.label)
					return -1;
				return 0;
			}

    return {
		
		clear: function() {
			for (i = 0; i < queryRows.length; ++i)
			{
				queryRows[i].token = '';
				queryRows[i].lexeme = '';
				queryRows[i].morph = '';
				queryRows[i].gramTag = '';
			}
			for (i = 0; i < metaFields.length; ++i)
				metaFields[i].value = '';
		}, 
			
        get: function(i) {
            return queryRows[i];
        },
		getMeta: function(i, place) {
			return metaFields[i * 3 + place];
		},
		getMetaAll: function() {
			return metaFields;
		}, 
		getMetaLength: function() {
			return metaFields.length - varietyAdded;
		},
		getMetaValues: function() {
			return metaValues;
		},
		getMetaChoices: function (index, place, negation = false) {
			constraints = {}
			choices = [];
			if (index == -1)
				name = 'location';
				//name = 'living-place';
			else
				name = metaFields[index * 3 + place].name.slice (5);
			for (var i = 0; i < metaFields.length; ++i)
				if (typeof metaFields[i].multiValue != 'undefined' && metaFields[i].multiValue.length > 0)
					constraints[metaFields[i].name.slice (5)] = [metaFields[i].multiValue, metaFields[i].negate];
			// console.log ('getMetaChoices, name:', name);
			// console.log ('getMetaChoices, constraints:', constraints);
			for (var i = 0; i < metaValues.length; ++i)
			{
				isMatch = true;
				for (c in constraints)
				{
					if (c == name)
						continue;
					if (match (metaValues[i], constraints[c][0], c) == constraints[c][1])
					{
						isMatch = false;
						break;
					}
				}
				if ((isMatch) && (metaValues[i][name] !== ""))
				{
					tmp = {}
					tmp['id_field'] = metaValues[i][name];
					tmp['label'] = metaValues[i][name];
					choices.push (tmp);
				}
			}
			var ch = choices.slice().sort(compare).reduce(function(a,b){if (a.length == 0 || a.slice(-1)[0]['id_field'] !== b['id_field']) a.push(b);return a;},[]);
			//console.log ('update:', index, place);
			//console.log ('choices:', ch);
			return choices.slice().sort(compare).reduce(function(a,b){if (a.length == 0 || a.slice(-1)[0]['id_field'] !== b['id_field']) a.push(b);return a;},[]);
		},
		getNegation: function() {
			return queryNegation;
		},
        getAll: function() {
            return queryRows;
        },
		getRowsNumber: function () {
			return queryRows.length;
		},
		getCqpChanged: function () {
			return cqpChanged;
		},
		forceLoadMV: function () {
			loadMetaData.getMetaFile('settings/meta_data.json').then(function(response) {
			metaValues = angular.fromJson(response);
			metaValuesLoaded = true;
			});	
		},
		isMetaLoaded: function () {
			return metaValuesLoaded;
		},
        set: function(index, item) {
            if (!(queryRows instanceof Array)) {
                queryRows = [];
            }
            queryRows[index] = item;
            return item;
        },
		setMeta: function(i, place, item) {
			metaFields[i * 3 + place] = item;
			return item;
		},
		addMetaMulti: function (i, place, item) {
			// console.log ('addMM, index, place, item, metaFields:', i, place, item, metaFields)
			metaFields[i * 3 + place].multiValue.push (item.id);
			// console.log ('adMM, metaFields changed:', metaFields);
			return item;
		},
		removeMetaMulti: function (i, place, item) {
			var index = metaFields[i * 3 + place].multiValue.indexOf (item.id);
			// console.log ('remMM, index, place, item, metaFields:', i, place, item, metaFields)
			// console.log ('index:', index);
			if (index > -1)
			{
				metaFields[i * 3 + place].multiValue.splice (index, 1);
				return item;
			}
			return;
		},
		setMetaMulti: function (i, place, item) {
			// console.log ('setMM, index, place, item, metaFields:', i, place, item, metaFields);
			metaFields[i * 3 + place].multiValue = [];
			for (j = 0; j < item.length; ++j)
			{
				metaFields[i * 3 + place].multiValue.push (item[j].id);
	
			}
			// console.log ('setMM, metaFields changed:', metaFields);
			return item;
		},
		setMetaVariety: function (variety) {
			for (var i = metaFields.length - 1; i >= 0; --i)
				if (metaFields[i].name == 'meta_variety')
					break;
			if (i < 0)
				return;
			var isSet = metaFields[i].multiValue.indexOf (variety);
			if (isSet > -1)
					metaFields[i].multiValue.splice (isSet, 1);
				else
					metaFields[i].multiValue.push (variety);
			$rootScope.$emit ('updateValues');
			return isSet;
		},
		setNegation: function(value) {
			queryNegation = value;
		},
		setMetaNegation: function(i, place, value) {
			metaFields[i * 3 + place].negate = value;
		},
		setLimit: function (index, place, value) {
			limit = [];
			for (i = 0; i < value.length; ++i)
				limit.push (value[i]);
		},
		getLimit: function () {
			return limit;
		},
		setCqpChanged: function (value) {
			cqpChanged = value;
		},
        pop: function() {
            if (queryRows.length > 1) queryRows.pop();
        },
        push: function() {
            if (!(queryRows instanceof Array)) {
                queryRows = [];
            }
            queryRows.push({
                from: '',
                to: '',
                token: '',
                lexeme: '',
				morph: '',
                gramTag: '',
                currentRowQueryStringWith: '',
                endWith: '',
                caseSensitive: false,
				ignoreDiacritics: true,
            });
        },
        length: function() {
            return queryRows.length;
        },
		metaLength: function() {
			return metaFields.length - varietyAdded;
		},
        prepareQuery: function() {
            var outputQuery = '';
            for (i = 0; i < queryRows.length; ++i) {
                if (typeof(queryRows[i]) == 'undefined') continue;
                var currentRowQueryString = '';
                queryRows[i].token = queryRows[i].token.replace(/ /g, "");
                queryRows[i].lexeme = queryRows[i].lexeme.replace(/ /g, "");
                queryRows[i].morph = queryRows[i].morph.replace(/ /g, "");
                queryRows[i].gramTag = queryRows[i].gramTag.replace(/ /g, "");
				
                if (queryRows[i].token != '') {
                    if (queryRows[i].endWith) currentRowQueryString += '.*';
                    currentRowQueryString += queryRows[i].token;
                    if (queryRows[i].beginWith) currentRowQueryString += '.*';

                    currentRowQueryString = 'word="' + currentRowQueryString + '"';
					var characterHandling = '';
                    if (queryRows[i].caseSensitive == false)
						characterHandling = 'c';
					if (queryRows[i].ignoreDiacritics == true)
						characterHandling += 'd';
                    if (characterHandling != '')
						currentRowQueryString += "%" + characterHandling;
                }
                if (queryRows[i].lexeme != '') {
                    if (currentRowQueryString != "") currentRowQueryString += " & ";
                    currentRowQueryString += ' lemma="' + queryRows[i].lexeme + '"';
                }
				if (queryRows[i].morph != '') {
					if (currentRowQueryString != '') currentRowQueryString += " & ";
					currentRowQueryString += ' morf="' + queryRows[i].morph + '"';
				}
                if (queryRows[i].gramTag) {
                    if (currentRowQueryString != "") currentRowQueryString += " & ";
                    currentRowQueryString += ' tag="' + queryRows[i].gramTag + '\"';
                }

                if (currentRowQueryString != '') {
					outputQuery += "[" + currentRowQueryString + "]";
                    if (queryRows[i].from < queryRows[i].to && queryRows[i].to > 0) {
                        outputQuery += '[]{' + (queryRows[i].from) + ',' + (queryRows[i].to) + '}';
                    }					
                }
				if ((queryRows[i].token != '' || queryRows[i].lexeme != '' || queryRows[i].gramTag != '') && queryNegation) outputQuery = '!' + outputQuery;
            }
			return outputQuery;

        },
		addMeta: function() {
			
			var metaList = [];
			for (i = 0; i < metaFields.length; ++i)
			{
				//console.log ('aadMeta:', metaFields[i]);
				if (metaFields[i].value != '')
					metaList.push ('match.' + metaFields[i].name + '="' + metaFields[i].value + '"');
				else if (metaFields[i].multiValue != []) {
					tmpList = [];
					var operators = ['="', ' | '];
					if (metaFields[i].negate == true)
						operators = ['!="', ' & '];
					//console.log ('ops:', operators);
					for (j = 0; j < metaFields[i].multiValue.length; ++j)
						tmpList.push ('match.' + metaFields[i].name + operators[0] + metaFields[i].multiValue[j] + '"');
					if (tmpList.length != 0)
					{
						statement = tmpList.join (operators[1]);
						if (tmpList.length > 1)
							statement = '(' + statement + ')';
						metaList.push (statement);
					}
				}
			}
			if (metaList.length != 0) return '::' + metaList.join(' & ');
			
			return '';
		},
    }

}]);

corpus.factory('stringProcessor', function() {
    return {
        removeSpaces: function(str) {
            if (typeof(str) == 'undefined') return '';
            var bufStr;
            var currentRowQueryString = 0;
            var end = str.length - 1;
            while (str[currentRowQueryString] == " ") currentRowQueryString++;
            while (str[end] == " ") end--;
            return str.substring(currentRowQueryString, end + 1);
        },
        replaceAndEscapeChrs: function(value) {
            if (typeof(value) == 'undefined') return '';
            value = value.replace(/\(/g, "\\(");
            value = value.replace(/\)/g, "\\)");
            value = value.replace(/\[/g, "\\[");
            value = value.replace(/\]/g, "\\]");
            value = value.replace(/\{/g, "\\}");
            value = value.replace(/\{/g, "\\}");
            value = value.replace(/\+/g, "\\+");
            value = value.replace(/\_/g, "\\_");
            value = value.replace(/\-/g, "\\-");
            value = value.replace(/\*/g, "\\*");
            value = value.replace(/\./g, "\\.");
            value = value.replace(/\:/g, "\\:");
            value = value.replace(/\;/g, "\\;");
            value = value.replace(/\,/g, "\\,");
            value = value.replace(/\?/g, "\\?");
            value = value.replace(/\!/g, "\\!");
            return value;
        }
    };
});

corpus.factory('autoCompleteDataService', ['$http', function() {
	
	var responseData = [];
	var lastQuery = '';
	
    return {
        getData: function(query, attribute) {
			
			query = query.replace(/ /g, "");
			if (query.length == 0)
				return responseData;
			query = '[' + attribute + '="' + query[0] + '.*"] ';
			if (query == lastQuery) 
				return responseData
			lastQuery = query;
			$http.get('autocomplete.php', {
				params: {query : query, attribute: at_type}
			})
			.success(function(data) {
				responseData = data;
			});
			
			return responseData;
		}
    };
}]);

corpus.factory('loadLanguages', ['$http', '$q', function(http, q) {
	
	return {
		getJsonFile: function(path) {
			var ldata;
			var deferred = q.defer();
			
			http.get(path).success (function(data) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}
	};
}]);

corpus.factory('loadMetaData', ['$http', '$q', function(http, q) {
	
	return {
		getMetaFile: function(path) {
			var mdata;
			var deferred = q.defer();
			
			http.get(path).success (function(data) {
				deferred.resolve(data);
			});
			return deferred.promise;
		}		
	};
}]);

/*
corpus.factory('configData', ['gettext', function(gettext) {
	
	var langs = {bg: gettext("Bulgarian"),
	 by: gettext("Belorussian"),
	 cz: gettext("Czech"),
	 hr: gettext("Croatian"),
	 mk: gettext("Macedonian"),
	 pl: gettext("Polish"),
	 ru: gettext("Russian"),
	 sk: gettext("Slovak"),
	 sl: gettext("Slovene"),
	 sr: gettext("Serbian"),
	 uk: gettext("Ukrainian"),
	 us: gettext("Upper Sorbian"),
	 da: gettext("Danish"),
	 de: gettext("German"),
	 en: gettext("English"),
	 nl: gettext("Dutch"),
	 no: gettext("Norwegian"),
	 sv: gettext("Swedish"),
	 es: gettext("Spanish"),
	 fr: gettext("French"),
	 it: gettext("Italian"),
	 pt: gettext("Portuguese"),
	 ro: gettext("Romanian"),
	 lt: gettext("Lithuanian"),
	 lv: gettext("Latvian"),
	 ee: gettext("Estonian"),
	 el: gettext("Greek"),
	 eo: gettext("Esperanto"),
	 fi: gettext("Finnish"),
	 hu: gettext("Hungarian"),
	hy: gettext("Armenian")};
	 
	return {
		getLangs: function (lang) {
			return langs[lang];
		}
	};
}]);
*/

corpus.filter ('matchTest', ['queryKeeper', function (queryKeeper) {
	return function (values, name, fl) {
		metaSet = queryKeeper.getMetaAll ();
		if (name == 'meta_location')
		//if (name == 'meta_living-place')
			fl = metaSet[0].value;
		nv = [];
		for (i = 0; i < values.length; ++i)
			if (values[i].indexOf (fl) != -1)
				nv.push (values[i]);
		return nv;
}}]);

/*
corpus.filter ('matchValues', ['queryKeeper', function (queryKeeper) {
	return function (values, name) {
		if (queryKeeper.getGuardian (name))
			return values;
		//queryKeeper.setGuardian (name)
		console.log ('sneeky!');
		var match = function (value, possibleValues, key) {
			// return false;
			var found = false;
			// if (name == 'meta_nationality')
				// console.log ('match:', value, possibleValues, key);
			needle = value[key.slice (5)];
			// if (name == 'meta_nationality')
				// console.log ('needle:', needle);
			for (var i = 0; i < possibleValues.length; ++i)
				if (needle == possibleValues[i])
				{
					console.log ('found');
					found = true;
					break;		
				}
			if (found) {
				// if (name == 'meta_nationality')
					// console.log ('return true');
				return true;
			}
			// if (name == 'meta_nationality')
				// console.log ('return false');
			return false;
		};
		metaSet = queryKeeper.getMetaAll ();
		metadata = queryKeeper.getMetaValues ();
		// console.log ('name:', name, values.length);
		// console.log ('mS:', metaSet);
		newValues = [];
		nV = [];
		constraints = {};
		// var found = [];
		for (var i = 0; i < metaSet.length; ++i) {
			if (typeof metaSet[i].multiValue != 'undefined' && metaSet[i].multiValue.length > 0)
				constraints[metaSet[i].name] = metaSet[i].multiValue;
			}
		// console.log ('metaSet:', metaSet);
		// console.log ('found:', found);
		// if (typeof found == 'undefined' || found.length == 0)
			// return values;
		// for (j = 0; j < values.length; ++j) {
			// for (i = 0; i < found.length; ++i)
				// if (found[i] == values[j][tag])
					// newValues.push (values[j]);					
		// }
		// console.log ('cn:', constraints);
		// console.log ('val:', values);
		for (var i = 0; i < values.length; ++i) {
			isMatch = true;
			for (c in constraints)
			{
				// console.log ('c:', c);
				if (match (values[i], constraints[c], c) == false)
				{
					isMatch = false;
					break;
				}
			}
			// console.log ('isMatch:', isMatch, i);
			if (isMatch)
				nV.push (values[i]);
		}		
		if (name == 'meta_natioonality') {
			console.log ("v:", values);
			console.log ("nV:", nV);
		}
		return nV;
	}
}]);*/