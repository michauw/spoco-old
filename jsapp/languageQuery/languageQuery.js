corpus.directive('languageQuery', ['stringProcessor', 'queryKeeper', 'gettextCatalog',  'ngDialog', function(stringProcessor, queryKeeper, gettextCatalog, ngDialog) {
    return {
        templateUrl: 'jsapp/languageQuery/languageQuery.html',
        restrict: 'E',
        scope: {
            language: '=',
        },
        controller: function($scope, $rootScope, gettextCatalog, gettext, queryKeeper, loadMetaData) {
			
			var lastQuery = '';
			var sourceFieldName = 'chapter_src';
			var genreFieldName = 'text_type';
			var map;
			var mapInitialized = false;
			var locations = [];
			var markers = [];
			queryKeeper.setCqpChanged(false);
			
			$scope.metaVisible = false;
			$scope.mapVisible = false;
			$scope.metaButton = 'Show';
			$scope.mapButton = 'Show map';
			
            /*$scope.fullNameOf = function(language){
                var translation = gettextCatalog.getString(configData.getLangs(language));
				if (gettextCatalog.getCurrentLanguage() == 'pl_PL' && translation.indexOf('i', translation.length - 1) !== -1)
					translation += 'ego';
                return translation;
            }*/
			
			var rad = function(x) {
			  return x * Math.PI / 180;
			};

			var getDistance = function(p1, p2) {
			  var R = 6378137; // Earth�s mean radius in meter
			  var dLat = rad(p2.lat() - p1.lat());
			  var dLong = rad(p2.lng() - p1.lng());
			  var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
				Math.cos(rad(p1.lat())) * Math.cos(rad(p2.lat())) *
				Math.sin(dLong / 2) * Math.sin(dLong / 2);
			  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
			  var d = R * c;
			  return d; // returns the distance in meter
			};
			
			var showMap = function () {
				// console.log ('in show');
				if (mapInitialized)
					google.maps.event.trigger (map, 'resize');
				else 
				{
					var props = getMapProps ();
					// console.log ('pr:', props);
					map = new google.maps.Map (document.getElementById ('inputMap'), props);
					for (var i = 0; i < locations.length; ++i)
					{
						var mProp = {
							position: new google.maps.LatLng (locations[i].latitude, locations[i].longitude),
							map: map,
							title: locations[i].name,
							icon: 'images/red_MarkerDot.png',
							color: 'red',
						}
						var marker = new google.maps.Marker (mProp);
						google.maps.event.addListener (marker, 'click', (function(marker, i) {
							return function() {
							  var negate = queryKeeper.getMeta (0, 1).negate;
							  if (marker.color == 'red')
							  {
								//console.log ('click, red');
								marker.setIcon ('images/purple_MarkerDot.png');
								marker.color = 'purple';
								//console.log ('marker changed to:', marker.color);
								if (negate)
									queryKeeper.removeMetaMulti (0, 1, {id: marker.title});
								else
									queryKeeper.addMetaMulti (0, 1, {id: marker.title});
								$rootScope.$emit ('updateValues');
								$rootScope.$emit ('updateLiving-Places');
							  }
							  else
							  {
								// console.log ('click, purple');
								marker.setIcon ('images/red_MarkerDot.png');
								marker.color = 'red';
								// console.log ('marker changed to:', marker.color);
								if (negate)
									queryKeeper.addMetaMulti (0, 1, {id: marker.title});
								else
									queryKeeper.removeMetaMulti (0, 1, {id: marker.title});
								$rootScope.$emit ('updateValues');
								$rootScope.$emit ('updateLiving-Places');
							  }
							}
						  })(marker, i));
						markers.push (marker);
					}
					mapInitialized = true;
					$rootScope.$emit ('updateValues');
					$rootScope.$emit ('updateLiving-Places');
				}
			}
			
			$rootScope.$on ('updateValues', function (event, negate, isLiving = false) {
				//console.log ('isLiving:', isLiving);
				//console.log ('get lp');
				//if (isLiving == "living-place")
				if (isLiving == "location")
					negate = false;
				var lp = queryKeeper.getMetaChoices (-1, -1, negate);
				// console.log ('done, lp:', lp);
				// console.log ('markers:', markers);
				var filteredLocations = [];
				
				for (var i = 0; i < markers.length; ++i)
				{
					var found = false;
					for (var j = 0; j < lp.length; ++j)
						if (markers[i].title == lp[j].id_field)
						{
							found = true;
							break;
						}
					if (found)
					{
						// console.log ('visible marker:', markers[i], i);
						markers[i].setVisible (true);
						filteredLocations.push ({latitude: markers[i].position.lat (), longitude: markers[i].position.lng()});
					}
					else
						markers[i].setVisible (false);
				}
				// console.log ('in event, locations:', filteredLocations);
				if (mapInitialized)
				{
					var props = getMapSettings (filteredLocations);
					map.setZoom (props.zoom);	
					map.setCenter (new google.maps.LatLng (props.latitude, props.longitude));
				}
			});
			
			$rootScope.$on ('updateMarkers', function (event, negate) {
				// console.log ('updateMarkers');
				var mv = queryKeeper.getMetaValues ();
				var colors = [negate ? 'red' : 'purple', negate ? 'purple' : 'red'];
				var selections = queryKeeper.getMeta (0, 1, negate).multiValue;
				for (var i = 0; i < markers.length; ++i)
				{
					var found = false;
					for (var j = 0; j < selections.length; ++j)
						if (markers[i].title == selections[j])
						{
							markers[i].setIcon ('images/' + colors[0] + '_MarkerDot.png');
							markers[i].color = colors[0];
							found = true;
							break;
						}
						if (!found)
						{
							markers[i].setIcon ('images/' + colors[1] + '_MarkerDot.png');
							markers[i].color = colors[1];
						}
				}
			});
			
			
			
			var prepareMap = function () {
				var latList = {mean: 0, min: 0, max: 0};
				var lngList = {mean: 0, min: 0, max: 0};
				var def_lat = 49;
				var def_lng = 22;
				
				var metaValues = queryKeeper.getMetaValues ();
				for (var i = 0; i < metaValues.length; ++i)
				{
					var lat = metaValues[i].gps_latitude;
					var lng = metaValues[i].gps_longitude;
					if (lat == "" || lng == "")
						continue;
					lat = Number (lat);
					lng = Number (lng);
					locations.push ({latitude: lat, longitude: lng, name: metaValues[i]["location"]});
				}
				
				
				//$scope.map.center = {latitude: latList.mean, longitude: lngList.mean};
				//$scope.map.zoom = 8;	
				// console.log ('prepare map - locations, object, type:', locations, locations[0].latitude, typeof locations[0].latitude);
				var props = getMapSettings (locations);
				return {
					center: new google.maps.LatLng (props.latitude, props.longitude),
					zoom: props.zoom,
					scaleControl:true,
					mapTypeId: google.maps.MapTypeId.TERRAIN,
				}
			}
			$scope.map = map;
			
			// $scope.$watch(function watchFunction(scope) {
					// if (!mapInitialized)
						// return;
					// return map.zoom;
				// }, function (nV) {
				// $scope.zoom = nV;
			// });
			var filterDuplicateMarkers = function (arr) {
				var tmpArr = {};

				for (var i = 0, len=arr.length; i < len; i++)
				{
					var obj = [arr[i]['latitude'], arr[i]['longitude']];
					tmpArr[obj] = arr[i];
				}

				arr = new Array();
				for (var key in tmpArr)
					arr.push(tmpArr[key]);
				
				return arr;
			}
			
			var flt = function (val) 
			{
				console.log ('flt, val:', val, val.latitude);
				return !isNaN (val.latitude);
			}
			
			var getMapSettings = function (positions) {
				// console.log ('positions:', positions);
				var mapHeight = 300;
				var span = 0.9
				var defLat = 49;
				var defLng = 22;
				var zoom;
				positions = positions.filter (flt);
				console.log ('pos filter:', positions);
				var maxLat = Math.max.apply (Math, positions.map (function (obj){return obj.latitude;}))
				var minLat = Math.min.apply (Math, positions.map (function (obj){return obj.latitude;}))
				var maxLng = Math.max.apply (Math, positions.map (function (obj){return obj.longitude;}))
				var minLng = Math.min.apply (Math, positions.map (function (obj){return obj.longitude;}))
				var amplitude = Math.abs (maxLat - minLat);
				// var distance = equator / 360 * amplitude;
				//console.log ('is diff:', positions[0].latitude == positions[1].latitude);
				if (filterDuplicateMarkers (positions).length < 2)
					zoom = 9;
				else
					zoom = Math.floor (Math.log2 (180 * (mapHeight / 256) / amplitude));
				var avgLat = (positions.length > 0) ? (maxLat + minLat) / 2 : defLat;
				var avgLng = (positions.length > 0) ? (maxLng + minLng) / 2 : defLng;
				// console.log ('map settings - positions.length, ampl, zoom, lat, lng:', positions.length, amplitude, zoom, avgLat, avgLng, maxLat, minLat, maxLng, minLng);
				return {zoom: zoom, latitude: avgLat, longitude: avgLng};
			}
			
			var getCenter = function (positions) {
				var filtered = {}
				var latSum = 0;
				var lngSum = 0;
				
				// console.log ('getCenter, positions:', positions);
				for (var i = 0; i < positions.length; ++i)
				{
					// console.log ('in loop, i:', i);
					filtered[positions[i].latitude.toString () + positions[i].longitude.toString ()] = positions[i];
				}
				// console.log ('filtered', filtered);
				var filteredLength = 0;
				for (loc in filtered)
				{
					filteredLength += 1;
					// console.log ('in loop, location, lat:', loc, filtered[loc].latitude);
					latSum += filtered[loc].latitude;
					lngSum += filtered[loc].longitude;
				}
				// console.log ('length:', filteredLength);
				var res = {lat: latSum / filteredLength, lng: lngSum / filteredLength};
				// console.log ('getCenter, filtered, res:', filtered, res);
				return {lat: latSum / filteredLength, lng: lngSum / filteredLength};
			}
						
			var getMapProps = function () {
				
				var meta = {};
				var props = {};
				// console.log ('isMLx:', queryKeeper.isMetaLoaded());
				if (!queryKeeper.isMetaLoaded ())
				{
					// console.log ('need to resolve meta');
					loadMetaData.getMetaFile('settings/meta_data.json').then(function(response) {
						props = prepareMap ();
						// console.log ('resolved, props:', props);
					});
				}
				else
				{
					// console.log ('meta already loaded...');
					props = prepareMap ();
					// console.log ('props:', props);
				}
				
				return props;
			}
			
			//$scope.map = {center: {latitude: 49, longitude: 22}, zoom: 8};
			//getMapProps ();
			
            
            $scope.exactPhraseQuery = function() {
                var quellText = stringProcessor.removeSpaces($scope.exactQuery);
                $scope.exactQuery = quellText;
                quellText = stringProcessor.replaceAndEscapeChrs(quellText);
                var resultText = "\"";
                var i = 0;
                // make from 'hallo you' query : '"hallo" "you"'
                while (i < quellText.length) {
                    var ch = quellText.charAt(i);
                    if (ch == ' ') {
                        resultText += "\"";
                    }
                    resultText += ch;
                    if (ch == ' ') {
                        resultText += "\"";
                    }
                    i++;
                }
                resultText += "\"";
                if ($scope.exactQueryCaseSensitive == true)
                    resultText = resultText + "%c";
                $scope.exactQueryOutput = resultText;
            }

            $scope.$watchCollection('{eq: exactQuery, cs: exactQueryCaseSensitive}', $scope.exactPhraseQuery);
			
			$scope.getVisible = function(element) {
				if (element == 'meta')
					return $scope.metaVisible;
				else if (element == 'map')
				{
					if ($scope.mapVisible)
						showMap ();
					return $scope.mapVisible;
				}
			}
			
			$scope.changeVisible = function(element) {
				if (element == 'meta')
				{
					$scope.metaVisible = !$scope.metaVisible;
					if ($scope.metaVisible == true)
						$scope.metaButton = gettext ('Hide');
					else
						$scope.metaButton = gettext ('Show');
				}
				else if (element == 'map')
				{
					$scope.mapVisible = !$scope.mapVisible;
					if ($scope.mapVisible == true)
						$scope.mapButton = gettext ('Hide map');
					else
						$scope.mapButton = gettext ('Show map');
				}
			}
			
			$scope.clear = function() {
				queryKeeper.clear();
			}

            $scope.moreRows = function() {
                queryKeeper.push();
				$scope.$broadcast ('newRow');
				var ii = document.getElementsByClassName("keyboardInput");
            };

            $scope.lessRows = function() {
                queryKeeper.pop();
            };

            $scope.rowsNumber = function() {
            	var myArr = [];
            	for (i=0; i<queryKeeper.length(); myArr.push(i++));
            	return myArr;
            }

            $scope.allRows = function(){
            	return queryKeeper.getAll();
            }
			
            $scope.getQuery = function(){
				if (queryKeeper.getCqpChanged ()) // if Cqp Search Field was manually edited, don't update it without user confirmation
				{
					return $scope.outputQuery;
				}
            	var query = queryKeeper.prepareQuery();
				
				if (query != '') query += queryKeeper.addMeta();
				
				return query;
            }
			
			$scope.checkManualUpdate = function() {
				queryKeeper.setCqpChanged(true);
			}
            
			$scope.getButtonState = function(element) {
				if (element == 'meta')
					return gettextCatalog.getString($scope.metaButton);
				else if (element == 'map')
					return gettextCatalog.getString ($scope.mapButton);
			}
			
			$scope.getMetaVisibility = function(){
				return "";
				
			}
			
			$scope.metaToShow = function()
			{
				var meta = queryKeeper.getMetaAll();
				var results = [];
				for (var i = 0; i < meta.length; ++i)
				{
					if (meta[i].inResults == true)
						results.push(meta[i].name);
				}
				return JSON.stringify(results);
			}
			
			$scope.queryNegation = queryKeeper.getNegation ();
			
			$scope.$watch('queryNegation', function(newValue) {
				queryKeeper.setNegation(newValue);
			});

            $scope.$watch('getQuery()',function(nval){
                $scope.outputQuery = nval;
            });
			
			
			$scope.updateQuery = function() {
			}

            $scope.exactQueryCaseSensitive = true;
			
			$scope.metaFieldsNumber = function() {
				return queryKeeper.metaLength();
			}
			
			$scope.metaFieldsNumberArray = function() {
				var tmpArray = [];
				for (i = 0; i < queryKeeper.metaLength() / 3; tmpArray.push(i++));
				return tmpArray;
			}
			
			$scope.changeFilter = function (lang_type) {
				var isSet = queryKeeper.setMetaVariety (lang_type);
				var button = document.getElementById ('filter_button_' + lang_type.toLowerCase ());
				if (isSet > -1)
					button.className = 'primary-button';
				else
					button.className = 'primary-button-light';				
			}
			
			$scope.resizeMap = function(){
				show = $scope.getVisible ('map');
				if (show)
				{
					google.maps.event.trigger($scope.map, 'resize');
					// console.log ('resizing');
				}
				return show;
			}
			
			//initialize query rows
			if (queryKeeper.getRowsNumber () == 0)
				$scope.moreRows();
        }
    };
}]);