corpus.directive('queryRow', ['queryKeeper', 'ngDialog', '$timeout', function(queryKeeper, ngDialog, $timeout) {
    return {
        templateUrl: 'jsapp/languageQuery/queryRow/queryRow.html',
        restrict: 'E',
        scope: {
            showTokensInBetween: '=',
            index: '=',
        },
		link: function ($scope, element, attributes) {
			$scope.$on ('newRow', function () {
				$timeout(function () {
					var inptoken = document.getElementById ('advToken_' + $scope.index);
					var inplemma = document.getElementById ('advLemma_' + $scope.index);
					VKI_attach (inptoken);
					VKI_attach (inplemma);
				}, 0, false);
			})
		},
        controller: function($scope, $rootScope) {
			
			var popUpOpened = false;
			var showWarning = true;
			
            $scope.queryRow = queryKeeper.get($scope.index);
            $scope.$watchCollection('queryRow', function(newValue, oldValue) {
				if (!showWarning)
					queryKeeper.setCqpChanged (false);
				if (queryKeeper.getCqpChanged() && !popUpOpened && showWarning)
				{
					popUpOpened = true;
					ngDialog.openConfirm({
						template: 'jsapp/languageQuery/queryRow/modal.html', 
						className: 'ngdialog-theme-default'
					}).then(function (hide) {
						queryKeeper.setCqpChanged (false);
						queryKeeper.set($scope, $scope.index, newValue);
						popUpOpened = false;
						showWarning = !hide;
					}, function (hide) {
						queryKeeper.set($scope, $scope.index, oldValue);
						popUpOpened = false;
						showWarning = !hide;					
					});
				}
				else
				{
					queryKeeper.set($scope.index, newValue);
				}
            });
			$scope.getFields = function (){
				return queryKeeper.get($scope, $scope.index);
			}
			$scope.autocmp = function(ids, atype) {
				$scope.atype = atype;
				getSuggestion (ids, atype, $scope);
			}
        }
    };
}]);