corpus.directive('metadata', ['queryKeeper', 'gettextCatalog', function(queryKeeper, gettextCatalog) {
    return {
        templateUrl: 'jsapp/languageQuery/metadata/metadata.html',
        restrict: 'E',
        scope: {
			index: '=',
			place: '='
        },
        controller: function($scope, $rootScope, $window, gettextCatalog, gettext) {
			
			var getValues = function () {
				values = [];
				metadata = queryKeeper.getMetaValues ();
				name = $scope.meta.name.slice (5);	
				for (i = 0; i < metadata.length; ++i)
					if (typeof metadata[i][name] != 'unknown')
					{
						var tmp = angular.copy (metadata)[i];
						tmp['name'] = name;
						tmp['id_field'] = metadata[i][name];
						tmp['label'] = tmp[name];
						values.push (tmp);						
					}
				values = values.slice().sort(compare).reduce(function(a,b){if (a.length == 0 || a.slice(-1)[0]['id_field'] !== b['id_field']) a.push(b);return a;},[]);
				return values;
			}
			
            $scope.meta = queryKeeper.getMeta($scope.index, $scope.place);
			
			$scope.getValues = function (negate) {
				// console.log ($scope.meta.name, ', metaChoices:', queryKeeper.getMetaChoices ($scope.index, $scope.place));
				return queryKeeper.getMetaChoices ($scope.index, $scope.place, negate);
			}
			$scope.values = $scope.getValues ();
			
			$scope.getMetaValues = function (){
				if ($scope.meta.name == 'meta_living-place')
					return check_matching (values, 'variety');
				else if ($scope.meta.name == 'meta_id')
					return check_matchin (values, 'living-place');
				return values;
			};
			$scope.model = [];
			$scope.negation = false;
			$scope.$watch ('negation', function (newValue) {
					queryKeeper.setMetaNegation ($scope.index, $scope.place, newValue);
					$rootScope.$emit ('updateValues', newValue, $scope.meta.name.slice (5));
					if ($scope.meta.name == 'meta_living-place')
						$rootScope.$emit ('updateMarkers', $scope.negation);
			});
			$scope.multiSettings = {showCheckAll: false, buttonClasses: 'btn btn-default meta', dynamicTitle: true, scrollable: true, scrollableHeight: '150px', idProp: 'id_field'};
			$scope.hint = {buttonDefaultText: gettextCatalog.getString($scope.meta.hint)};
			// console.log ('hint:', $scope.meta.hint, 'trans:', gettextCatalog.getString($scope.meta.hint));
			$scope.$watchCollection ('model', function (newValue, oldValue) {
				//console.log ('nV:', newValue);
				queryKeeper.setMetaMulti ($scope.index, $scope.place, newValue);
				$rootScope.$emit ('updateValues', $scope.negation);
				if ($scope.meta.name == 'meta_living-place')
				{
					// console.log ('emitting...');
					$rootScope.$emit ('updateMarkers', $scope.negation);
				}
				//$scope.values = queryKeeper.getMetaChoices ($scope.index, $scope.place);
			});
			$rootScope.$on ('updateValues', function (event, negate) {
				 //console.log ('updating, negation:', negate, $scope.meta.name);
				// console.log ('name, model:', $scope.meta.name, $scope.model);
				$scope.values = $scope.getValues (negate);
			});
			$rootScope.$on ('updateLiving-Places', function (event) {
				//console.log ('up lp');
				if ($scope.meta.name == 'meta_living-place')
				{
					var selections = queryKeeper.getMeta ($scope.index, $scope.place).multiValue;
					$scope.model = [];
					for (var i = 0; i < selections.length; ++i)
						$scope.model.push ({id: selections[i]});
				}
			});
			$rootScope.$on ('gettextLanguageChanged', function (event) {
				$scope.hint = {buttonDefaultText: gettextCatalog.getString($scope.meta.hint)};
				console.log ($scope.hint);
			});

			$scope.getTip = function () {
				return gettextCatalog.getString('show') + ' <b> ' + $scope.meta.hint + '</b> ' + gettextCatalog.getString('in results');
			}
			
        }
    };
}]);