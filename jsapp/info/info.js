corpus.directive('infoProject', ['stringProcessor', 'queryKeeper', 'gettextCatalog',  'ngDialog', function(stringProcessor, queryKeeper, gettextCatalog, ngDialog) {
    return {
        templateUrl: 'jsapp/info/project.html',
        restrict: 'E',
        scope: {
            language: '=',
        },
    };
}]);

corpus.directive('infoTeam', ['stringProcessor', 'queryKeeper', 'gettextCatalog',  'ngDialog', function(stringProcessor, queryKeeper, gettextCatalog, ngDialog) {
    return {
        templateUrl: 'jsapp/info/team.html',
        restrict: 'E',
        scope: {
            language: '=',
        },
    };
}]);

corpus.directive('infoPublications', ['stringProcessor', 'queryKeeper', 'gettextCatalog',  'ngDialog', function(stringProcessor, queryKeeper, gettextCatalog, ngDialog) {
    return {
        templateUrl: 'jsapp/info/publications.html',
        restrict: 'E',
        scope: {
            language: '=',
        },
    };
}]);

corpus.directive('infoStatistics', ['stringProcessor', 'queryKeeper', 'gettextCatalog',  'ngDialog', function(stringProcessor, queryKeeper, gettextCatalog, ngDialog) {
    return {
        templateUrl: 'jsapp/info/statistics.html',
        restrict: 'E',
        scope: {
            language: '=',
        },
    };
}]);

corpus.directive('infoInstruction', ['stringProcessor', 'queryKeeper', 'gettextCatalog',  'ngDialog', function(stringProcessor, queryKeeper, gettextCatalog, ngDialog) {
    return {
        templateUrl: 'jsapp/info/instruction.html',
        restrict: 'E',
        scope: {
            language: '=',
        },
    };
}]);

corpus.directive('infoContact', ['stringProcessor', 'queryKeeper', 'gettextCatalog',  'ngDialog', function(stringProcessor, queryKeeper, gettextCatalog, ngDialog) {
    return {
        templateUrl: 'jsapp/info/contact.html',
        restrict: 'E',
        scope: {
            language: '=',
        },
    };
}]);