<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
    <xsl:output method="html"/>
    <xsl:template match="/">
        <html>
            <head>
                <link rel="stylesheet" href="CSS/form.css" type="text/css" />
                <link rel="stylesheet" href="CSS/feedback.css" type="text/css" />
                <link rel="stylesheet" href="CSS/fontello.css" type="text/css" />
                <link rel="stylesheet" href="CSS/jquery-ui-1.8.17.custom.css" type="text/css" />
				 <!--<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>-->
                <script type="text/javascript">
                    $(document).ready(function() {
                    $('td').click(function() {
                    var myCol = $(this).index();
                    var $tr = $(this).closest('tr');
                    var myRow = $tr.index();
                    var table = $(this).closest('table');
                    var lang = $(table).find("tr").eq(1).find("td").eq(myCol).html();
                    var primlang = $(table).find("tr").eq(1).find("td").eq(0).html();
                    var token_nr = $(table).find("tr").eq(myRow).find("td").eq(0).find("font").eq(0).html();
                    var corpus = $(table).find("i").eq(0).html();
                    var url = 'context_xml.php?corpus=' + corpus + '&amp;lang=' + lang + '&amp;primlang=' + primlang + '&amp;token_nr=' + token_nr;
                    var windowName = corpus + "_" + primlang + ":" + token_nr + " in " + corpus + "_" + lang;
                    var windowSize = "width=300,height=400,scrollbars=yes";
                    window.open(url, windowName, windowSize);
                    });
                    });
                </script>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>
                <script type="text/javascript" src="js/feedback.js"></script>
            </head>
            <body style="padding-top: 111px;">
                <a href = "#" id="logo"> <img style=" z-index:10; position:fixed; opacity:0.4; top:2px; left:5px;" src="images/iconOrigin.png"></img></a>
                <div class = "gradient-title shadow" style=" z-index:9; width:99.8%; top:0px; left:1px; position:fixed; height:74px; padding-top:5px; border-style:solid; border-width:1px; border-color:#663300;">
                    <a href = "http://www.unibe.ch/"> <img style="width:200px; z-index:10; opacity:0.6; margin-top:8px; margin-right:10px; float:right" src="images/Dreierlogo.gif"></img> </a>
                    <!--<img style="width:40px; z-index:10; opacity:0.4; margin-top:10px; margin-right:23px; float:right" src="images/and.png"></img>-->
                   
                    
                    <div style="top:5px; font-family: 'Times New Roman', serif;font-size:20px;margin-left:250px;" > 
                        <h3 style="color:#663300;font-style:normal;">Corpus results </h3>
                        <em id="title" style="font-family:slavicfont; font-size:25px"></em>
                    </div>
                </div>
                <xsl:apply-templates/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="RESULTS/text()">
        <xsl:element name="b">
            <xsl:value-of select="'('"/>
            <xsl:value-of select="."/>
            <xsl:value-of select="' hit'"/>
            <xsl:if test=".&gt;1">
                <xsl:value-of select="'s'"/>
            </xsl:if>
            <xsl:value-of select="'.)'"/>
					
        </xsl:element>
        <xsl:element name="p"/>
    </xsl:template>
		
    <xsl:template match="CONCORDANCE">
        <table id="ausgabetabelle" class="gradient shadow" style="font-size: small; border-spacing: 1; background-color: #FFEEAA; width: 100%;"> 
            <tr>
                <td colspan="3">
                                    <xsl:element name="b">
                    <xsl:attribute name="center">right</xsl:attribute>
                        <xsl:attribute name="style">font-family:'Times New Roman',serif;font-size:1.4em;color:#663300;</xsl:attribute>
                        <xsl:value-of select="'Resultlist for revision of changes by users. '"/>
                    </xsl:element>									
                        <xsl:value-of select="'Note that changes take effect only after reencoding the corpus.'"/>

                </td>
                <td>
                    <xsl:attribute name="align">right</xsl:attribute>
                    <xsl:attribute name="style">font-weight:bold;font-family:'Times New Roman',serif;font-size:1.2em;color:#663300;</xsl:attribute>
                    <xsl:value-of select="' '"/>
                    <xsl:variable name="ALLnum-LINE" select="count(//LINE)"/>
                    <xsl:value-of select="$ALLnum-LINE"/>
                    <xsl:value-of select="' hits overall'"/>
                </td>
            </tr>
					<!-- Überschrift -->
            <xsl:element name="tr">
               <!-- <xsl:element name="td">-->
                <xsl:value-of select="/RESULTS/@primlang"/>
                <!-- </xsl:element> -->
                <xsl:variable name="fpos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[2]/following-sibling::ALIGN)"/>
                <xsl:for-each select="LINE[1]/following-sibling::ALIGN[position() &lt;= $fpos]">
                   <!-- <xsl:element name="td"> -->
                    <xsl:value-of select="substring-after(@name, '_')"/>
                    <!-- </xsl:element> -->
                </xsl:for-each>
            </xsl:element>
					
            <xsl:variable name="npos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[1]/following-sibling::LINE/following-sibling::ALIGN)"/>
            <xsl:variable name="numberColumns" select="count (LINE[1]/following-sibling::ALIGN[position() &lt;= $npos])"/>


					
            <xsl:for-each select="LINE">
                <xsl:element name="tr">
                    <xsl:variable name="pos" select="position()" />
                    <xsl:attribute name="id">
                        <xsl:value-of select="$pos"/>
                    </xsl:attribute>
                    <xsl:if test="(position() mod 2)=1">
                        <xsl:attribute name="style">color: #663300; background-color: #FFFFFF;</xsl:attribute>
                    </xsl:if>
                    <xsl:if test="(position() mod 2)=0">
                        <xsl:attribute name="style">color: #663300; background-color: #FFDDAA;</xsl:attribute>
                    </xsl:if>
					
					
                    <!-- <xsl:element name="td">-->
                    <xsl:apply-templates/>
                    <!-- </xsl:element> -->
                    <xsl:for-each select="following-sibling::ALIGN[position() &lt;= $npos]">
                        <xsl:element name="td">
                            <xsl:attribute name="width">
                                <xsl:value-of select="(100 div ($numberColumns+1))"/>
                                <xsl:value-of select="'%'"/>
                            </xsl:attribute>
					<!-- 					<xsl:value-of select="position()"/>  -->
                            <xsl:attribute name="title"></xsl:attribute>
                            <xsl:apply-templates/>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:element>
            </xsl:for-each>
        </table>					
    </xsl:template>



    <xsl:template match="MATCHNUM">
        <xsl:element name="td">
            <xsl:attribute name="style">width:100px</xsl:attribute>
            <font color="red">
                <xsl:apply-templates/>
                <!--xsl:element name="a">
                    <xsl:attribute name="class">icon-doc-text-inv</xsl:attribute>
                    <xsl:attribute name="title">show context</xsl:attribute>
                    <xsl:attribute name="style">color:red;</xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('./context_xml.php?&amp;token_nr=', .)"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:value-of select="'_blank'"/>
                    </xsl:attribute>
                </xsl:element>-->
                <xsl:element name="a">
                    <xsl:attribute name="class">icon-doc-text-inv</xsl:attribute>
                    <xsl:attribute name="title">save link / show context</xsl:attribute>
                    <xsl:attribute name="style">color:blue;</xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('./context_xml_byTimeID.php?&amp;from=', following-sibling::STRUCS/from, '&amp;file=', following-sibling::STRUCS/file, '&amp;to=', following-sibling::STRUCS/to)"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:value-of select="'_blank'"/>
                    </xsl:attribute>
                </xsl:element>
                <xsl:element name="a">
                    <xsl:attribute name="class">icon-doc-text-inv</xsl:attribute>
                    <xsl:attribute name="title">copy CSV</xsl:attribute>
                    <xsl:attribute name="style">color:green;</xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('./context_xml_byMatchID.php?&amp;match=', .)"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:value-of select="'CSV'"/>
                    </xsl:attribute>
                </xsl:element>
            </font>
        </xsl:element>
    </xsl:template>

    <xsl:template match="MATCH">
        <font color="red" >
            <b>
                <xsl:apply-templates/>
            </b>
        </font>
    </xsl:template>

    <xsl:template match="STRUCS">
        <xsl:element name="td">
            <xsl:attribute name="style">width:100px</xsl:attribute>
            <xsl:value-of select="concat('Speaker: ', spkr, ' ')"/>

                
            <xsl:element name="a">
                <xsl:attribute name="class">icon-headphones</xsl:attribute>
                <xsl:attribute name="title">audio file link</xsl:attribute>
                <xsl:attribute name="href">
                    <xsl:value-of select="concat('./OUT/','')"/>
                    <xsl:value-of select="concat(file, '-',from,'-', to, '.wav')"/>
                </xsl:attribute>
            </xsl:element>
			
			<xsl:value-of select="'Type: '"/>	
			<xsl:choose>
				<xsl:when test="starts-with(file, 'TRA')">
					<xsl:value-of select="'Ukrainian '"/>
				</xsl:when>
				<xsl:when test="starts-with(file, 'LEM')">
					<xsl:value-of select="'Lemko '"/>
				</xsl:when>
				<xsl:when test="starts-with(file, 'SLO')">
					<xsl:value-of select="'Slovakian '"/>
				</xsl:when>
			</xsl:choose>
        
<!--<xsl:value-of select="concat('File included: ', date, ' ')"/>-->

            <xsl:element name="br">
            </xsl:element>
            <xsl:element name="audio">
                <xsl:attribute name="controls"/>

                <xsl:attribute name="preload">none</xsl:attribute>
                <xsl:element name="source">
                    <xsl:attribute name="src">
                        <xsl:value-of select="concat('./OUT/','')"/>
                        <xsl:value-of select="concat(file, '-',from,'-', to, '.wav')"/>
                    </xsl:attribute>
                    <xsl:attribute name="type">audio/wav</xsl:attribute>
                </xsl:element>
            </xsl:element>


        </xsl:element>
    </xsl:template>
    <!--  create div over content -->
    <xsl:template match="CONTENT">
            <xsl:variable name="from" select="../STRUCS/fromId" />
            <xsl:variable name="to" select="../STRUCS/toId" />
            <xsl:variable name="file" select="../STRUCS/file" />
            <xsl:variable name="usr" select="../STRUCS/usr" />
            <xsl:variable name="sts" select="../STRUCS/sts" />
	    <xsl:variable name="audiofile" select="concat('./OUT/',../STRUCS/file, '-',../STRUCS/from,'-', ../STRUCS/to, '.wav')"/>
        <xsl:element name="td">
            <xsl:apply-templates/>
        </xsl:element>
        <xsl:element name="td">       
	    
	<xsl:value-of select="$usr"/>        
            <i id="icons" class="icon-ok-circle editButton" title="Approve entry as is!" onclick="checkInTheChanges($(this),'{$from}','{$to}','{$file}')"/>
            <nbs/><nbs/><nbs/>
            <i id="icons" class="icon-edit editButton" title="See history, edit and approve entry" onclick="openEditDialog($(this),'{$from}','{$to}','{$file}','{$usr}','{$audiofile}')"/>
            <xsl:choose>
                <xsl:when test="(($sts='unapproved') or ($sts='new'))">
                    <i id="icons" class="icon-alert alertButton" title="Last change was not yet approved!"/>
                </xsl:when>
                <xsl:when test="$sts='ok'">
                    <i id="icons" class="icon-ok okButton" title="Change was approved!"/>
                </xsl:when>
            </xsl:choose>
        </xsl:element>
    </xsl:template>    
    
    <xsl:template match="TOKEN">
        <xsl:element name="font">
            <xsl:attribute name="title">
                <xsl:value-of select="ANNOT"/>
            </xsl:attribute>
            <xsl:value-of select="text()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="s">
        <xsl:element name="sup">
            <xsl:value-of select="@id"/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
