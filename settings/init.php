<?php
/**
 * all initial information, paths, globals	
 * created on 16.02.2007 by Roland Meyer
 */

// $OS = "macosx"
$OS = "linux";


$CWBDIR = "";
$PARCORPUSDIR = "/data/Rogovatka/CORPUSCWB/";
$REGISTRY = "/data/Rogovatka/CORPUSCWB/Registry";
$CORPUSNAME = "ROGOVATKA";
$METAPATH = "settings/meta.json";
$METADATA = "settings/meta_data.json";
$EXCLUDE_RESTRICTED = true;
$pathToTheElanFiles = "/data/Rogovatka/ELAN-FILES/";

$CQPINIT = "settings/cqpinit";
$HARDBOUNDARY = "999";
$ENCODING = "UTF-8";

// only relevant for XML-based concordance
$pstructures = 'set ShowTagAttributes on; set PrintStructures "utterance, utterance_from, utterance_to, utterance_file, utterance_spkr, utterance_fromId, utterance_toId, utterance_usr, utterance_sts, utterance_date, meta_gps_latitude, meta_gps_longitude"; ';
$metastructure = json_decode (file_get_contents ($METAPATH));

if (count($metastructure) > 0)
{
	$ANNOTCONTEXT .= ' set PrintStructures "';
	foreach ($metastructure as $field)
		$ANNOTCONTEXT .= $field->name.', ';
	$ANNOTCONTEXT = rtrim ($ANNOTCONTEXT, ", ");
	$ANNOTCONTEXT .= '";';
}
$RESTRICTED = [];
if ($EXCLUDE_RESTRICTED == true)
{
	$metadata = json_decode (file_get_contents ($METADATA));
	foreach ($metadata as $metaObject)
	{
		if (isset ($metaObject->restricted) && $metaObject->restricted == "r")
			array_push ($RESTRICTED, $metaObject->id);
	}
}
?>
