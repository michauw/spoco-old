<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
    <xsl:variable name="GRAPHIC-HEADER">
                    <div class = "gradient-title shadow" style=" z-index:9; width:99.8%; top:0px; left:1px; position:fixed; height:74px; padding-top:5px; border-style:solid; border-width:1px; border-color:#663300;">
                    <!--<a href = "http://www.unibe.ch/"> <img style="width:90px; z-index:10; opacity:0.4; margin-top:1px; margin-right:10px; float:right" src="images/unibern_logo2.png"></img> </a>-->
                    <!--<img style="width:40px; z-index:10; opacity:0.4; margin-top:10px; margin-right:23px; float:right" src="images/and.png"></img>-->
                    <!--<a href = "http://www.hse.ru/en/"> <img style=" z-index:10; opacity:0.8; margin-top:1px; margin-right:10px; height:70px; float:right" src="images/hse_logo.png"></img> </a>-->
                 
                    <div style="top:5px; font-family: 'Times New Roman', serif;font-size:20px;margin-left:250px;" > 
                         <h3 style="color:#663300;font-style:normal;"><!-- title -->SpoCo Corpus<!-- title --></h3>
                         <!--<em id="title" style="font-family:slavicfont; font-size:25px"></em>-->
                    </div>
                </div>
     </xsl:variable>
             

    <xsl:variable name="CORPUSHITSLABEL">
                    <xsl:element name="b">
                        <xsl:attribute name="style">font-family:'Georgia',serif;font-size:1.4em;color:black;</xsl:attribute>
                        <xsl:value-of select="'Query results'"/>
                    </xsl:element>									
     </xsl:variable>

     
    <xsl:variable name="CONTEXTHITSLABEL">
                    <xsl:element name="b">
                        <xsl:attribute name="style">font-family:'Times New Roman',serif;font-size:1.4em;color:#663300;</xsl:attribute>
                        <xsl:value-of select="'Context view'"/>
                    </xsl:element>									
     </xsl:variable>
     
     
     
     
     

     
</xsl:stylesheet>
