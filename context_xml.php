<?php 
	include('include/init.php'); 
$token_nr = $_GET["token_nr"];
// retrieve defaults
$CQPOPTIONS = " ";
if ($CQPINIT) {
	$CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
	$CQPOPTIONS .= " -b $HARDBOUNDARY";
}


$pstructures='set ShowTagAttributes on; set PrintStructures "utterance, utterance_from, utterance_to, utterance_file, utterance_spkr"; ';
$showtags=' show +tag; show +lemma; ';

$query2 = ' Go = []; cat Go '.$token_nr.' '.$token_nr.'';

$execstring = "$CWBDIR" . "cqpcl -r $REGISTRY" . "  '".$CORPUSNAME."; set Context 10 utterance; set PrintMode sgml;  ". $showtags. $pstructures. $query2 .";'";

	$execstring .= " | sed -r 's/&lt;utterance_from ([^&]*?)&gt;/<from>\\1<\/from>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_to ([^&]*?)&gt;/<to>\\1<\/to>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_file ([^&]*?)&gt;/<file>\\1<\/file>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_spkr ([^&]*?)&gt;/<spkr>\\1<\/spkr>/g'";
	$execstring .= " | sed -r 's/&lt;(\/)?utterance&gt;/<utterance\/>/g'";
	$execstring .= " | sed -r 's/&lt;(\/)?utterance&gt;/<utterance\/>/g'";
//	$execstring .= " | sed -r 's/&lt;line\_nr\s+(\d+)&gt;&lt;column\_nr\s+(\d+)&gt;&lt;page\_nr\s+(\d+)&gt;/<hit page=\"\\3\" column=\"\\2\" line=\"\\1\"\/>/g'";
//	$execstring .= "' | sed -r 's/(&|&amp;)lt;(\/)?s(_id [0-9]+)?(&|&amp;)gt;//g'";
//	$execstring .= " | sed -r 's/^<align (.*)$/<ALIGN \\1 <\/ALIGN>/g'";
	$execstring .= " | sed -r 's/\/__UNDEF__//g'";
	$execstring .= " | sed -r 's/(<TOKEN>[^\/<]+)\/([^>]+)<\/TOKEN>/\\1<ANNOT>\\2<\/ANNOT><\/TOKEN>/g'";
//	$execstring .= " | sed -r 's/&(amp;)+(quot;|amp;)/\&\\2/g'";
	$execstring .= " | sed -r 's/<attribute[^>]+>//g'";
	$execstring .= "\n";

	unset($out);
	exec($execstring, $out);
$outstr = trim(implode("\n", $out));
$outstr = '<RESULTS>'.$outstr  .  '</RESULTS>';
	header('Content-type: text/xml; charset=utf-8'); 
	echo('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="./parallel-kwic.xsl" ?>');
	echo ($outstr);
?>
