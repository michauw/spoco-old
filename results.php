<?php
//session_regenerate_id();
//ini_set("session.use_cookies","1");
include('settings/init.php');
/**
 * read out all POSTed values and write them to appropriate vars
 */
// quiqsearch: query -> "query."
//$_POST['query'] = str_replace( '"', '', $_POST['query'] );
$acttexts = isset($_POST['acttexts']) ? $_POST['acttexts'] : array();
$langs = isset($_POST['langs']) ? $_POST['langs'] : array();
$primlang = isset($_POST['primlang']) ? $_POST['primlang'] : array();
$kontextnum = isset($_POST['kontextnum']) ? $_POST['kontextnum'] : 10;
$kontexttyp = isset($_POST['kontexttyp']) ? $_POST['kontexttyp'] : "word";

foreach ($acttexts as $x) {
    if ($_POST['selText_' . $x]) {
        foreach ($_POST['selText_' . $x] as $y) {
            $selectedTexts[$x][$y] = 1;
            ksort($selectedTexts[$x]);
            $selectedLanguages[$y] = 1;
        };
    };
};
ksort($selectedTexts);

if (get_magic_quotes_gpc()) {
    foreach (array_keys($selectedLanguages) as $x) {
        if ($_POST['query_' . $x]) {
            $query[$x] = stripslashes($_POST['query_' . $x]);
        };
    };
} else {
    foreach (array_keys($selectedLanguages) as $x) {
        if ($_POST['query_' . $x]) {
            $query[$x] = $_POST['query_' . $x];
        };
    };
};


    include('results_xml.php');
?>
