<?php

// retrieve defaults
$CQPOPTIONS = " ";
if ($CQPINIT) {
    $CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
    $CQPOPTIONS .= " -b $HARDBOUNDARY";
}

$query = $_POST["query"];

if (!empty ($_POST["langFilter"]))
	$query .= '::';
foreach ($_POST["langFilter"] as $filter)
{
	if (!(strrpos ($query, '::') == strlen ($query) - 2))
		$query .= ' | ';
	$query .= 'match.utterance_file="' . $filter . '.*"';
}
$pstructures = 'set ShowTagAttributes on; set PrintStructures "utterance, utterance_from, utterance_to, utterance_file, utterance_spkr, utterance_fromId, utterance_toId, utterance_usr, utterance_sts, utterance_date, meta_gps_latitude, meta_gps_longitude, meta_living-place"; ';



$showtags = ' show +tag; show +lemma; show +morf; ';
$execstring = "$CWBDIR" . "cqpcl -r $PARCORPUSDIR" . "Registry '" . $CORPUSNAME ."; set Context 1 utterance; set PrintMode sgml;  " . $showtags . $pstructures . $query . ";'";
$execstring .= " | sed -r 's/&lt;utterance_from ([^&]*?)&gt;/<from>\\1<\/from>/g'";
$execstring .= " | sed -r 's/&lt;utterance_to ([^&]*?)&gt;/<to>\\1<\/to>/g'";
$execstring .= " | sed -r 's/&lt;utterance_file ([^&]*?)&gt;/<file>\\1<\/file>/g'";
$execstring .= " | sed -r 's/&lt;utterance_spkr ([^&]*?)&gt;/<spkr>\\1<\/spkr>/g'";
$execstring .= " | sed -r 's/&lt;utterance_fromId ([^&]*?)&gt;/<fromId>\\1<\/fromId>/g'";
$execstring .= " | sed -r 's/&lt;utterance_toId ([^&]*?)&gt;/<toId>\\1<\/toId>/g'";
$execstring .= " | sed -r 's/&lt;utterance_sts ([^&]*?)&gt;/<sts>\\1<\/sts>/g'";
$execstring .= " | sed -r 's/&lt;utterance_usr ([^&]*?)&gt;/<usr>\\1<\/usr>/g'";
$execstring .= " | sed -r 's/&lt;utterance_date ([^&]*?)&gt;/<date>\\1<\/date>/g'";
$execstring .= " | sed -r 's/&lt;(\/)?utterance&gt;/<utterance\/>/g'";
$execstring .= " | sed -r 's/&lt;(\/)?utterance&gt;/<utterance\/>/g'";
$execstring .= " | sed -r 's/&lt;meta_gps_latitude ([^&]*?)&gt;/<gps_latitude>\\1<\/gps_latitude>/g'";
$execstring .= " | sed -r 's/&lt;meta_gps_longitude ([^&]*?)&gt;/<gps_longitude>\\1<\/gps_longitude>/g'";
$execstring .= " | sed -r 's/&lt;meta_living-place ([^&]*?)&gt;/<living-place>\\1<\/living-place>/g'";
$execstring .= " | sed -r 's/\/__UNDEF__//g'";
$execstring .= " | sed -r 's/(<TOKEN>[^\/<]+)\/([^>]+)<\/TOKEN>/\\1<ANNOT>\\2<\/ANNOT><\/TOKEN>/g'";
$execstring .= " | sed -r 's/<attribute[^>]+>//g'";
$execstring .= "\n";

unset($out);

exec($execstring, $out);

$outstr = trim(implode("\n", $out));
$outstr = '<RESULTS>' . $outstr . '</RESULTS>';

if (isset($_POST['btn']['xml'])) {
    header('Content-type: text/xml; charset=utf-8');
    echo('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="./parallel-kwic.xsl" ?>');
  echo ($outstr);
} 
if (isset($_POST['btn']['xmlfile'])) {
    header('Content-type: text/xml; charset=utf-8');
    header('Content-Disposition: attachment; filename="results.xml"');
    echo('<?xml version="1.0" encoding="UTF-8"?>' . $outstr); 
};
if (isset($_POST['btn']['csvfile'])) {
    header('Content-type: text/xml; charset=utf-8');
    echo('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="./export-csv.xsl" ?>');
    echo ($outstr);
};
if (isset($_POST['btn']['csvnewfile'])) {
    header('Content-type: text/xml; charset=utf-8');
    echo('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="./export-csv-sanja.xsl" ?>');
    echo ($outstr);
};
?>
