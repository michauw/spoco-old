<?php 
	include('settings/init.php'); 
$from = $_GET["from"];
$to = $_GET["to"];
$file = $_GET["file"];
// retrieve defaults
$CQPOPTIONS = " ";
if ($CQPINIT) {
	$CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
	$CQPOPTIONS .= " -b $HARDBOUNDARY";
}


$pstructures='set ShowTagAttributes on; set PrintStructures "utterance_from, utterance_to, utterance_file, utterance_spkr,utterance,  utterance_fromId, utterance_toId, utterance_usr, utterance_sts, utterance_date"; ';
$showtags=' show +tag; show +lemma; show +utterance_spkr; show +utterance_to; show +utterance_from;';

$query2 = ' <utterance_from="'.$from.'"><utterance_to="'.$to.'"><utterance_file="'.$file.'">[] expand to utterance';

$execstring = "$CWBDIR" . "cqpcl -r ".$REGISTRY."  '".$CORPUSNAME."FULL;  set Context 10 utterance; set PrintMode sgml;  ". $showtags. $pstructures. $query2 .";'";

	$execstring .= " | sed -r 's/&lt;utterance_from ([^&]*?)&gt;/<from>\\1<\/from>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_to ([^&]*?)&gt;/<to>\\1<\/to>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_file ([^&]*?)&gt;/<file>\\1<\/file>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_spkr ([^&]*?)&gt;/<spkr>\\1<\/spkr>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_fromId ([^&]*?)&gt;/<fromId>\\1<\/fromId>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_toId ([^&]*?)&gt;/<toId>\\1<\/toId>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_sts ([^&]*?)&gt;/<sts>\\1<\/sts>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_usr ([^&]*?)&gt;/<usr>\\1<\/usr>/g'";
	$execstring .= " | sed -r 's/&lt;utterance_date ([^&]*?)&gt;/<date>\\1<\/date>/g'";

	$execstring .= " | sed -r 's/&lt;(\/)?utterance&gt;/<utterance\/>/g'";
	$execstring .= " | sed -r 's/&lt;(\/)?utterance&gt;/<utterance\/>/g'";
	$execstring .= " | sed -r 's/&lt;(\/)?utterance_spkr&gt;//g'";
	$execstring .= " | sed -r 's/&lt;(\/)?utterance_from&gt;//g'";
	$execstring .= " | sed -r 's/&lt;(\/)?utterance_to&gt;//g'";
	$execstring .= " | sed -r 's/&lt;(\/)?utterance_file&gt;//g'";
	$execstring .= " | sed -r 's/&lt;(\/)?utterance_(file|toId|fromId|sts|usr|date)&gt;//g'";
	$execstring .= " | sed -r 's/(<TOKEN>)(<from>\S*?<\/from><to>\S?*<\/to><spkr>\S?*<\/spkr>)/\\2\\1/g'";
//	$execstring .= " | sed -r 's/&lt;line\_nr\s+(\d+)&gt;&lt;column\_nr\s+(\d+)&gt;&lt;page\_nr\s+(\d+)&gt;/<hit page=\"\\3\" column=\"\\2\" line=\"\\1\"\/>/g'";
//	$execstring .= "' | sed -r 's/(&|&amp;)lt;(\/)?s(_id [0-9]+)?(&|&amp;)gt;//g'";
//	$execstring .= " | sed -r 's/^<align (.*)$/<ALIGN \\1 <\/ALIGN>/g'";
	$execstring .= " | sed -r 's/\/__UNDEF__//g'";
	$execstring .= " | sed -r 's/(<TOKEN>[^\/<]+)\/([^>]+)<\/TOKEN>/\\1<ANNOT>\\2<\/ANNOT><\/TOKEN>/g'";
//	$execstring .= " | sed -r 's/&(amp;)+(quot;|amp;)/\&\\2/g'";
	$execstring .= " | sed -r 's/<attribute[^>]+>//g'";
	$execstring .= "\n";

	unset($out);
	exec($execstring, $out);

$outstr = trim(implode("\n", $out));
$outstr = '<RESULTS>'.$outstr  .  '</RESULTS>';
	header('Content-type: text/xml; charset=utf-8'); 
	echo('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="./parallel-kwic-context-NEW.xsl" ?>');
	echo ($outstr);
?>
