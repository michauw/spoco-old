<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="de_DE" xml:lang="de_DE">
<!--
 * Created on 10.01.2007
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
				-->
<head>
	<meta http-equiv="content-type" content="text/html; charset=<?php echo $ENCODING; ?>" />
	<title><?php echo $PAGE_TITLE; ?></title>
	<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="js/checkAdmin.js"></script>
</head>
<body>
	 <h3><?php echo $PAGE_HEADLINE; ?></h3>
	 <p> <?php echo $PAGE_CONTENT; ?> </p>
	<?php echo $FORM_DATA; ?>
	<?php
		echo '
				<div id="textsform"></div>
				<div id="languageform"></div>
				<br class="clear" />

<h1>Admin page - revise and approve changes</h1>
<form action="results_xml_adminInterface.php" method="post" target="_blank">

Before revision, it is recommended to reencode the corpus based on the current ELAN-Files including all user-supplied changes.</p> To do this, press this button and wait until the page loads (DO NOT PRESS REPEATEDLY):
<input type="submit" class="submit" name="btn[reEncodeCorpus]" width="100" onClick="return confirm(\'Do you REALLY want to reencode the corpus? If so, press OK. WAIT. DO NOT PRESS THE BUTTON A SECOND TIME WHILE THE PAGE IS LOADING.\')" value=" Reencode corpus "/>
	<br />
	<br />
	<br />

To list and revise all changes, press here: <input type="submit" class="submit" name="btn[allNew]" width="100" value=" List all unconfirmed changes "/>
	<br />
	<br />


You may also enter a CQP query here to search for specific changes:

<br />
 <input type="text"   style="width:700px;" name="query" />
<br />


<br />

				<!--
				<input type="submit" class="submit" name="btn[conc]" width="100" value=" KWIC concordance "/>
				<input type="submit" class="submit" name="btn[xml]" width="100" value=" XML display "/>
				<input type="submit" class="submit" name="btn[xmlfile]" width="100" value=" XML download "/><br />
				<input type="submit" class="submit" name="btn[conc]" width="100" value=" Export CSV "/>
				-->
				<input type="submit" class="submit" name="btn[searchNew]" width="100" value=" Search "/>
				<br />
<br />
				</form><br class="clear" />';
	?>
</body>
</html>
