#!/usr/bin/env python
import sys
import libxml2

def main(argv):
    fileName = argv[0];
    left = argv[1];
    right = argv[2];
    print "" + fileName + " " + left + " " + right;
    import os
    if not os.path.exists (fileName):
        print 'bum!'
    doc = libxml2.parseFile(fileName)
    nodes=doc.xpathEval("/ANNOTATION_DOCUMENT/TIER/ANNOTATION/ALIGNABLE_ANNOTATION[@TIME_SLOT_REF1='"+left+"'][@TIME_SLOT_REF2='"+right+"']/ANNOTATION_VALUE")
    commentNodes=doc.xpathEval("/ANNOTATION_DOCUMENT/TIER/ANNOTATION/ALIGNABLE_ANNOTATION[@TIME_SLOT_REF1='"+left+"'][@TIME_SLOT_REF2='"+right+"']/COMMENT_VALUE")
    
    #newElement = nodes[0].newChild(None, 'counter', None)
    #newElement.newProp('count', '1')
    print '|'.join(map(lambda x: x.serialize(), nodes)) + '|'+'|'.join(map(lambda x: x.serialize(), commentNodes))
    doc.freeDoc()
    
if __name__ == "__main__":
   main(sys.argv[1:])