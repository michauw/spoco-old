#!/usr/bin/env python
import sys
import libxml2
import time, datetime

def main(argv):
    fileName = argv[0];
    left = argv[1];
    right = argv[2];
    feedback = argv[3];
    usr = argv[4];
    sts = argv[5];
    comment = argv[6];
    ts = time.time()
    tStamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S');
    print "" + fileName + " " + left + " " + right + " " + feedback +" " + usr + " " +sts + " " + tStamp;
    
    doc = libxml2.parseFile(fileName)
    f = open(fileName,'w')
    
    root = doc.xpathEval("/ANNOTATION_DOCUMENT/TIER/ANNOTATION/ALIGNABLE_ANNOTATION[@TIME_SLOT_REF1='"+left+"'][@TIME_SLOT_REF2='"+right+"']")
    #root = doc.getRootElement()
    newNode = libxml2.newNode("ANNOTATION_VALUE")
    newNode.setContent(feedback)
    newNode.setProp('usr', usr)
    newNode.setProp('sts', sts)
    newNode.setProp('tStamp', tStamp)
    root[0].addChild(newNode)
    newNodeComment = libxml2.newNode("COMMENT_VALUE")
    newNodeComment.setContent(comment)
    newNodeComment.setProp('usr', usr)
    newNodeComment.setProp('tStamp', tStamp)
    root[0].addChild(newNodeComment)
    doc.saveTo(f)
    f.close
    doc.freeDoc()
    
if __name__ == "__main__":
   main(sys.argv[1:])