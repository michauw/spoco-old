<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
    <xsl:output method="html"/>
    <xsl:include href="settings/corpus-specific.xsl"/>
    <xsl:template match="/">
        <html>
            <head>
			<title>Query results</title>
				<link rel="stylesheet" href="bootstrap.css" type="text/css" />
				<style>body { padding-top: 66px; max-width: 100%; }</style>

                <link rel="stylesheet" href="css/form.css" type="text/css" />
                <link rel="stylesheet" href="css/feedback.css" type="text/css" />
                <link rel="stylesheet" href="css/fontello.css" type="text/css" />
                <link rel="stylesheet" href="css/jquery-ui-1.8.17.custom.css" type="text/css" />
				<script src="https://maps.googleapis.com/maps/api/js"></script>
				<script src="maps.js"></script>
				<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
                <script type="text/javascript">
                    $(document).ready(function() {
             /*       $('td.text').click(function() {
                    var myCol = $(this).index();
                    var $tr = $(this).closest('tr');
                    var myRow = $tr.index();
                    var table = $(this).closest('table');
                    var lang = $(table).find("tr").eq(1).find("td").eq(myCol).html();
                    var primlang = $(table).find("tr").eq(1).find("td").eq(0).html();
                    var token_nr = $(table).find("tr").eq(myRow).find("td").eq(0).find("font").eq(0).html();
                    var corpus = $(table).find("i").eq(0).html();
                    var url = 'context_xml.php?corpus=' + corpus + '&amp;lang=' + lang + '&amp;primlang=' + primlang + '&amp;token_nr=' + token_nr;
                    var windowName = corpus + "_" + primlang + ":" + token_nr + " in " + corpus + "_" + lang;
                    var windowSize = "width=300,height=400,scrollbars=yes";
                    window.open(url, windowName, windowSize);
                    });*/
                    });
					
					 
					 function post(path, params, method) {
							method = method || "post"; // Set method to post by default if not specified.
							console.log (params.length);
							// The rest of this code assumes you are not using a library.
							// It can be made less wordy if you use one.
							var form = document.createElement("form");
							form.setAttribute("method", method);
							form.setAttribute("action", path);

							for(var key in params) {
								if(params.hasOwnProperty(key)) {
									var hiddenField = document.createElement("input");
									hiddenField.setAttribute("type", "hidden");
									hiddenField.setAttribute("name", key);
									hiddenField.setAttribute("value", params[key]);

									form.appendChild(hiddenField);
								 }
							}
							document.body.appendChild(form);
							form.submit();
						}

						  var allChecked = false;
						  var checkAll = function () {
						    console.log ('check all');
							$('input.check').attr('checked',!allChecked)
							allChecked = !allChecked;
							if (allChecked === true)
								$('#check_button').attr('value', 'uncheck all');
							else
								$('#check_button').attr('value', 'check all');
						  };
						  var exportCsv = function () {
							var selected = [];
							var langs = [];
							$('tr').first().next().find('td').each (function (){
								langs.push ($(this).text());
							});
							selected.push (langs);
							var trs = $('input.check:checked').closest ('tr');
							$(trs).each (function (){
								tds = $(this).find ('td:not(:first-child)');
								console.log (tds.length);
								tdtexts = [];
								$(tds).each (function (){
									tdtexts.push ($(this).text ().replace(/^\s+|\s+$/g, ''))
								});
								selected.push (tdtexts);
							});
							var jselected = JSON.stringify (selected);
							post ('export_csv.php', {selected: jselected});
						  }					
                </script>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.17/jquery-ui.min.js"></script>
                <!--<script type="text/javascript" src="js/feedback.js"></script>-->
				<style>
				.control-button {
					margin: 1px; 
					margin-bottom: 5px; 
					box-shadow: 1px 1px 1px;
				}
				.no-space {margin-left: -4px;}
				td {
				padding: 5px;
				font-size: 14px;
				}
				</style>

		<script type="text/javascript">
				function play_sound(s) {
						document.getElementById(s).play();
				}
		</script>

            </head>
            <body>
	  <div role="navigation" class="navbar navbar-fixed-top navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a href="." class="navbar-brand"><xsl:value-of select="$GRAPHIC-HEADER" /></a>
        </div>
			<div style="text-align: right; padding-top: 24px; padding-right: 40px;">
			<a href="." style="color: black; text-decoration: none;">back to the search page >></a>
			</div>
        </div>
      </div>
				<div class="container" style="margin-top: 40px;">
				<main role="main">
				<div class="row marketing">
                <xsl:apply-templates/>
				</div>
				</main>
				</div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="RESULTS/text()">
        <xsl:element name="b">
            <xsl:value-of select="'('"/>
            <xsl:value-of select="."/>
            <xsl:value-of select="' hit'"/>
            <xsl:if test=".&gt;1">
                <xsl:value-of select="'s'"/>
            </xsl:if>
            <xsl:value-of select="'.)'"/>
					
        </xsl:element>
        <xsl:element name="p"/>
    </xsl:template>
		
    <xsl:template match="CONCORDANCE">
		<!--<div style="margin-bottom: 5px">
			
			<input type="button" id="check_button" class="control-button"  value="select all" onclick="checkAll()"/>
			<input type="button" id="export_csv" class="control-button"  value="export selected results (csv)" onclick="exportCsv()"/>
			<input type="button" id="export_txt" class="control-button"  value="export selected results (txt)" onclick="export()"/>
		</div>-->

		<table style="font-size: small; width: 100%; border-color: grey;"> 
            <tr style="background-color: #FFFFFF; border-radius: 5px;">
                <td colspan="2">
					<xsl:attribute name="style">font-family:'Georgia',serif;color: black;font-size: 15px;</xsl:attribute>
                	<xsl:copy-of select="$CORPUSHITSLABEL"/>
                </td>
                <td>
                    <xsl:attribute name="align">right</xsl:attribute>
                    <xsl:attribute name="style">font-weight:bold;font-family:'Georgia',serif;color: black;font-size: 20px;</xsl:attribute>
                    <xsl:value-of select="' '"/>
                    <xsl:variable name="ALLnum-LINE" select="count(//LINE)"/>
                    <xsl:value-of select="$ALLnum-LINE"/>
                    <xsl:value-of select="' hit(s)'"/>
                </td>
            </tr>
					<!-- Überschrift -->
		</table>
        <table style="font-size: small; border-spacing: 1px; background-color: gray; width: 100%; border-color: grey;"> 
            <xsl:element name="tr">
               <!-- <xsl:element name="td">-->
                <xsl:value-of select="/RESULTS/@primlang"/>
                <!-- </xsl:element> -->
                <xsl:variable name="fpos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[2]/following-sibling::ALIGN)"/>
                <xsl:for-each select="LINE[1]/following-sibling::ALIGN[position() &lt;= $fpos]">
                   <!-- <xsl:element name="td"> -->
                    <xsl:value-of select="substring-after(@name, '_')"/>
                    <!-- </xsl:element> -->
                </xsl:for-each>
            </xsl:element>
					
            <xsl:variable name="npos" select="count (LINE[1]/following-sibling::ALIGN) - count (LINE[1]/following-sibling::LINE/following-sibling::ALIGN)"/>
            <xsl:variable name="numberColumns" select="count (LINE[1]/following-sibling::ALIGN[position() &lt;= $npos])"/>


					
            <xsl:for-each select="LINE">
                <xsl:element name="tr">
                    <xsl:variable name="pos" select="position()" />
                    <xsl:attribute name="id">
                        <xsl:value-of select="$pos"/>
                    </xsl:attribute>
                    <xsl:if test="(position() mod 2)=1">
                        <xsl:attribute name="style">color: black; background-color: #eae9e5;</xsl:attribute>
                    </xsl:if>
                    <xsl:if test="(position() mod 2)=0">
                        <xsl:attribute name="style">color: black; background-color: white;</xsl:attribute>
                    </xsl:if>
					
					
                    <!-- <xsl:element name="td">-->
					<xsl:element name="td">
						<xsl:attribute name="style">
							width:1%;
						</xsl:attribute>
						<xsl:element name="input">
							<xsl:attribute name="type">checkbox</xsl:attribute>
							<xsl:attribute name="class">check</xsl:attribute>
						</xsl:element>
					</xsl:element>					
                    <xsl:apply-templates/>
                    <!-- </xsl:element> -->
                    <xsl:for-each select="following-sibling::ALIGN[position() &lt;= $npos]">
                        <xsl:element name="td">
                            <xsl:attribute name="width">
                                <xsl:value-of select="(100 div ($numberColumns+1))"/>
                                <xsl:value-of select="'%'"/>
                            </xsl:attribute>
					<!-- 					<xsl:value-of select="position()"/>  -->
                            <xsl:attribute name="title"></xsl:attribute>
                            <xsl:apply-templates/>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:element>
            </xsl:for-each>
        </table>					
    </xsl:template>



    <xsl:template match="MATCHNUM">
        <xsl:element name="td">
            <xsl:attribute name="style">width:12%</xsl:attribute>
			<xsl:attribute name="class">text</xsl:attribute>
            <font>
                <xsl:apply-templates/>
                <!--xsl:element name="a">
                    <xsl:attribute name="class">icon-doc-text-inv</xsl:attribute>
                    <xsl:attribute name="title">show context</xsl:attribute>
                    <xsl:attribute name="style">color:red;</xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('./context_xml.php?&amp;token_nr=', .)"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:value-of select="'_blank'"/>
                    </xsl:attribute>
                </xsl:element>-->
                <xsl:element name="a">
                    <xsl:attribute name="class">icon-doc-text-inv</xsl:attribute>
                    <xsl:attribute name="title">show context</xsl:attribute>
                    <xsl:attribute name="style">color:blue;</xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('./context_xml_byTimeID.php?&amp;from=', following-sibling::STRUCS/from, '&amp;file=', following-sibling::STRUCS/file, '&amp;to=', following-sibling::STRUCS/to)"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:value-of select="'_blank'"/>
                    </xsl:attribute>
                </xsl:element>
                <!--<xsl:element name="a">
                    <xsl:attribute name="class">icon-doc-text-inv</xsl:attribute>
                    <xsl:attribute name="title">copy CSV</xsl:attribute>
                    <xsl:attribute name="style">color:green;</xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:value-of select="concat('./context_xml_byMatchID.php?&amp;match=', .)"/>
                    </xsl:attribute>
                    <xsl:attribute name="target">
                        <xsl:value-of select="'CSV'"/>
                    </xsl:attribute>
                </xsl:element>-->
            </font>
        </xsl:element>
    </xsl:template>

    <xsl:template match="MATCH">
        <font color="red" >
            <b>
                <xsl:apply-templates/>
            </b>
        </font>
    </xsl:template>

    <xsl:template match="STRUCS">
        <xsl:element name="td">
            <xsl:attribute name="style">width:20%</xsl:attribute>
			<xsl:attribute name="class">text</xsl:attribute>
            <xsl:value-of select="concat('Speaker: ', spkr, ' ')"/>
			<!--
            <xsl:element name="a">
                <xsl:attribute name="target">metadata</xsl:attribute>
                <xsl:attribute name="title">click to access informant meta data</xsl:attribute>
                <xsl:attribute name="href">https://docs.google.com/spreadsheets/d/1um7RQJswVwYcPI-acUIZUDpnFEiSHmIipEmhWf3Vhz0/pubhtml?gid=0&amp;single=true</xsl:attribute>
            <xsl:value-of select="concat(spkr, ' ')"/>
            </xsl:element>
			-->
            <xsl:element name="a">
                <xsl:attribute name="class">icon-headphones</xsl:attribute>
                <xsl:attribute name="title">audio file link</xsl:attribute>
                <xsl:attribute name="href">
                    <xsl:value-of select="concat('./OUT/','')"/>
                    <xsl:value-of select="concat(file, '-',from,'-', to, '.wav')"/>
                </xsl:attribute>
            </xsl:element>
			<!--<xsl:value-of select="concat('Living place: ', location, ' ')"/>-->
			<!--<xsl:element name="a">
				<xsl:attribute name="title">show map</xsl:attribute>
				<xsl:attribute name="href">
					<xsl:value-of select="'Map'"/>
				</xsl:attribute>
				<xsl:attribute name="onclick">
				</xsl:attribute>
				Map
			</xsl:element>-->
			<!--<a href="javascript:">
				<xsl:attribute name="onclick">loadMapWindow('<xsl:value-of select="gps_latitude"/>', '<xsl:value-of select="gps_longitude"/>', '<xsl:value-of select="living-place"/>')</xsl:attribute>
				Map
			</a>-->
			
			<xsl:if test="date='xxx'">
				<xsl:value-of select="concat('File included: ', date, ' ')"/>
			</xsl:if>

            <xsl:element name="br">
            </xsl:element>
            <xsl:element name="audio">
                <xsl:attribute name="controls"/>

                <xsl:attribute name="preload">none</xsl:attribute>
                <xsl:element name="source">
                    <xsl:attribute name="src">
                        <xsl:value-of select="concat('./OUT/','')"/>
                        <xsl:value-of select="concat(file, '-',from,'-', to, '.wav')"/>
                    </xsl:attribute>
                    <xsl:attribute name="type">audio/wav</xsl:attribute>
                </xsl:element>
            </xsl:element>


        </xsl:element>
    </xsl:template>
    <!--  create div over content -->
    <xsl:template match="CONTENT">
        <xsl:element name="td">  
			<xsl:attribute name="style">
				width:68%
			</xsl:attribute>
			<xsl:attribute name="class">text</xsl:attribute>
            <xsl:variable name="from" select="../STRUCS/fromId" />
            <xsl:variable name="to" select="../STRUCS/toId" />
            <xsl:variable name="file" select="../STRUCS/file" />
            <xsl:variable name="usr" select="../STRUCS/usr" />
            <xsl:variable name="sts" select="../STRUCS/sts" />
            <xsl:variable name="audiofile" select="concat('./OUT/',../STRUCS/file, '-',../STRUCS/from,'-', ../STRUCS/to, '.wav')"/>
            <i id="icons" class="icon-edit editButton" title="Edit Entry" onclick="openEditDialog($(this),'{$from}','{$to}','{$file}','{$usr}', '{$audiofile}')"/>
            <xsl:choose>
                <xsl:when test="$sts='new'">
                    <i id="icons" class="icon-alert alertButton" title="Last change was not yet approved!"/>
                </xsl:when>
                <xsl:when test="$sts='ok'">
                    <i id="icons" class="icon-ok okButton" title="Change was approved!"/>
                </xsl:when>
            </xsl:choose>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>    
    
    <xsl:template match="TOKEN">
        <xsl:element name="font">
			<xsl:variable name="punctuation" select="'. , : ; ? !'" />
			<xsl:if test="contains(concat(' ', $punctuation, ' '), concat(' ', text(), ' ')	)">
				<xsl:attribute name="class">
					no-space
				</xsl:attribute>
			</xsl:if>		
            <xsl:attribute name="title">
                <xsl:value-of select="ANNOT"/>
            </xsl:attribute>
            <xsl:value-of select="text()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="s">
        <xsl:element name="sup">
            <xsl:value-of select="@id"/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>
