<?php

// Autocomplete funktioniert folgendermassen: In UI(UserInterface) f�ngt beim jeden Tastendr�ck in den Eingabefeldern javascript die eingegeben W�rter ab.
// Relevant ist nur die erste Buchstabe von jedem Wort. Und daruas wird Query generiert, die an Backend bzw. diese Datei geschickt wird. 
// So zum Besipel: aus "ich gehe"  wird "i.* g.*". Diese Query wird hierher geschickt und hier wird anfrage an die Suchmschine generiert.
// Query wird nur dann an Backend geschickt, wenn sie ge�ndert wird.
// So zum Beispiel: wen wir aus "ich gehe" "ich ge" machen query wird nicht geschickt, da beides ("i.* g.*"), dagegen "ich gehe essen" wird geschickt, da ("i.* g.* e.*")
// Nach dem die Anfrage an die Suchmaschine unten generiert wurde und an sie geschickt, bekommen wir eine Anwort (gesuchte W�rter) von der Suchmaschine, die wir Parsen und 
// mit json an UI schicken, wo es abgefangen wird und in automplete-database geladen. Autocomplete-library ist von jquery

include('include/init.php');
// retrieve defaults
$CQPOPTIONS = " ";
session_start();


$query = $_GET['query'];
list($id, $file, $from, $to, $text, $usr, $sts, $comment) = explode("|", $query);
//$pathToTheElanFiles = "/data/Ruthenian/ELAN-FILES";
if ($_SESSION['angemeldet']) {
    $execstring = "python python/updateFeedback.py " . $pathToTheElanFiles . $file . ".eaf " . $from . " " . $to . " '" . $text . "' '" . $_SESSION['username'] . "' " . $sts . " '".$comment."'";
    $outstr = "";
    exec($execstring, $outstr);
    echo json_encode($id . "|" . $file . "|" . $from . "|" . $to . "|" . $_SESSION['username'] . "|" . serialize($_SESSION['status']));
} else {
    echo json_encode("no_user");
}
?>
