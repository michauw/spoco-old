<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fn="http://www.w3.org/2003/11/xpath-functions">
	<xsl:output method="text"/>
	<xsl:strip-space elements="*"/>
<xsl:template match="TOKEN">
<xsl:value-of select="text()"/>
<xsl:if test="not (position() = last() or starts-with(following-sibling::TOKEN[1], '.') or starts-with(following-sibling::TOKEN[1], ',') or starts-with(following-sibling::TOKEN[1], ':') or starts-with(following-sibling::TOKEN[1], '?') or starts-with(following-sibling::TOKEN[1], '!'))"> 
<xsl:value-of select="' '"/></xsl:if>
</xsl:template>

<xsl:template match="LINE">
<xsl:value-of select="concat(MATCHNUM, '&#9;', STRUCS/spkr, '&#9;',STRUCS/file,'&#9;')"/>
<xsl:value-of select="concat('OUT/',STRUCS/file, '-',STRUCS/from,'-', STRUCS/to, '.wav','&#9;')"/>
<xsl:apply-templates select="CONTENT/MATCH/preceding-sibling::TOKEN"/>
<xsl:value-of select="' #'"/>
<xsl:apply-templates select="CONTENT/MATCH/TOKEN"/>
<xsl:value-of select="'# '"/>
<xsl:apply-templates select="CONTENT/MATCH/following-sibling::TOKEN"/>
<xsl:value-of select="'&#9;'"/>
<xsl:apply-templates select="CONTENT/MATCH/TOKEN"/>
<xsl:value-of select="'&#10;'"/>
</xsl:template>



</xsl:stylesheet>
