$(document).ready(function() {
    //document.getElementById('title').innerHTML='\u0412\u0435\u043B\u0438\u043A\u0438\u0435\ \u041C\u0438\u043D\u0435\u0438\ \u0427\u0435\u0442\u044C\u0438\ ';
    var screenWidth = $(window).width();
    var screenHeight = $(window).height() - 167;
    var infoWidth = screenWidth - 70;
    var imageWidth= screenWidth;
    imageWidth*=0.5;
    imageWidth+="px";
    var imageMarg= screenWidth;
    imageMarg*=0.2;
    imageMarg+="px";
    //alert(document.getElementById('searchFields').style.width + " " + infoWidth);
    //document.getElementById('infoBigBox').style.width=infoWidth + "px";
    document.getElementById('infoBigBox').style.height=screenHeight + "px";
	
    // Welcom dialog for more information you must search in google for "dialog jquery"
    var strWelcom = "<div style='width: 85%'><p>The Corpus of Spoken Rusyn Language is a collection of Rusyn vernacular speech from different regions of the Carpathians. The recordings were made in Poland, Slovakia, Ukraine, and Hungary in 2015.  The corpus is a result of the <a href=\"http://www.dfg.de\">DFG</a>-funded research project “Rusyn as a minority language across state borders: dynamic processes” at Friedrich-Schiller-University in Jena, Germany. For further information on the project, please visit <a href=\"http://www.russinisch.uni-jena.de\">http://www.russinisch.uni-jena.de</a>.</p>" +

			"<p><h5>Principal investigator, specifications: </h5>Achim Rabus<h5>Overall concept, specifications, consulting:</h5>Ruprecht von Waldenfels (Cracow, Berkeley)<br><h5>Programming:</h5>Michał Wozniak (Cracow)<br><br>Based on previous work by Simon Skilevič and Ruprecht von Waldenfels<br><h5>Researcher:</h5>Andrianna Schimon<h5>Research assistant:</h5>Yuriy Remestvenskyy<br><h5>Transcription:</h5>Andrianna Schimon, Yuriy Remestvenskyy, Anna</p></div>";
    document.getElementById('infoBox').style.height=screenHeight - 50 + "px";
    $("#infoBox").html(strWelcom);
});
