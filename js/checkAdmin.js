user_global="";
user_status="guest";
//get user information
$( document ).ready(function(){
    $.ajax({
            url : 'checkAuthentication.php',
            dataType : 'json',
            data: {
                query : "checkAuthentication"
            }, 
            success: updateUserInformation
    });
});
function updateUserInformation(data, textStatus, jqXHR)
{ 
    if (data.toString().indexOf("no_user")>=0)
        $("body").html("YOU ARE NOT ALLOWED TO VIEW THIS PAGE!!!");
    else
    {
        var tmp = data.split("|");
        user_global = tmp[1];
        user_status = tmp[2];
//alert(user_status)
        if (user_status !== "admin")
        {
           $("body").html("YOU ARE NOT ALLOWED TO VIEW THIS PAGE!!!");
        }
     }
}