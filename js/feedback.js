user_global="";
user_status="guest";
globalLineId="";
globalFile="";
globalFrom="";
globalTo="";
pressed=false;
//get user information
$( document ).ready(function(){
    $.ajax({
            url : 'checkAuthentication.php',
            dataType : 'json',
            data: {
                query : "checkAuthentication"
            }, 
            success: updateUserInformation
    });
});
function updateUserInformation(data, textStatus, jqXHR) 
{ 
    if (data.toString().indexOf("no_user")>=0)
		{}
    else
    {
        var tmp = data.split("|");
        user_global = tmp[1];
        user_status = tmp[2];
        if (user_status == "admin" || user_status=="user")
        {
           $(".editButton").css("visibility","visible");
        }
        if (user_status == "admin" )
        {
           $(".adminPageButton").css("visibility","visible");
        }
    }
}
function checkInTheChanges(obj, from, to, file)
{
	
    $(obj).css("color", "rgb(0, 204, 0)"); 
    pressed=true;
    var lineId = $(obj).parent().parent().attr('id');
    getFeedback(lineId, file, from, to); 
    globalLineId = lineId;
    globalFile = file;
    globalFrom = from;
    globalTo = to;
}
function openEditDialog(obj, from, to, file, usr,audiofile)
{
    var lineId = $(obj).parent().parent().attr('id')
    var idDialog = "dialog_" + lineId;
    if ($('#'+idDialog).length==0) 
    {
        $("<div id = "+idDialog+" ></div>").dialog();
        $('#'+idDialog).dialog('option', 'title', 'Update Entry Dialog');
        $(".ui-dialog-titlebar").css("font-size", "1.2em")
        $('#'+idDialog).parent().css('position', 'fixed');
        $('#'+idDialog).parent().css("width","1000px");
        $('#'+idDialog).parent().css("top","100px");
        $('#'+idDialog).parent().css("left","100px");
        //create change history of this entry 
        var hystoryTable = "<div><div class='dialogTable'><table class='dialogTableTable'>"
        hystoryTable+= "<tr><th>previous correction</th></tr>"
        hystoryTable += "</table></div>";
        hystoryTable += '<div id="instructions">Please edit entry as appropriate and save. Note that changes take effect only after reencoding the corpus (usually, overnight). </div>';
        if (user_status=="admin")
        {
            hystoryTable += "<input style='width:945px' id='inputFeedback' type='input' value=''>"
        } 
        else
        {
            hystoryTable += "<input id='inputFeedback' type='input' style='width:945px' value=''>"
        }

	hystoryTable+='</br><audio id="audiobox" controls="" preload="none" loop="true"><source src="'+audiofile+'" type="audio/wav"></source></audio>';


        hystoryTable += "<input id='updateFeedBtn' type='button' value='Save corrected entry' onClick=updateFeedback("
                        +lineId + ",'"
                        +file + "','"
                        +from + "','"
                        +to + "','" 
                        + user_global + "','unapproved')>";
        if (user_status=="admin")
        {
            hystoryTable += "<input id='commitFeedBtn' type='button' value='Validate corrected entry' onClick=updateFeedback("
                    + lineId + ",'"
                    + file + "','"
                    + from + "','"
                    + to + "','"
                    + user_global + "','ok')>";
        }
	


        // add coments field
        hystoryTable += "<textarea title='Describe your changes' onClick='emptyComment(this)' id='inputComents'>Comments</textarea>"
        hystoryTable += "</div>";
        $('#'+idDialog).html(hystoryTable);
    } 
    else
    {
        //if dialog was allready created, then just open it
        $('#'+idDialog).dialog('open');
        $('#'+idDialog).parent().css('position', 'fixed');
        $('#'+idDialog).parent().css("width","1000px");
        $('#'+idDialog).parent().css("top","100px");
        $('#'+idDialog).parent().css("left","100px");
    } 
    getFeedback(lineId, file, from, to); 
}
function emptyComment(obj)
{
    obj.innerHTML = "";
}
// Ajax-connector between updateFeedback.php and UserInterface
function updateFeedback(id, file, from, to, usr, sts)
{
    var text = $("#dialog_"+id).find('#inputFeedback').val();
    var comment = ""+$("#dialog_"+id).find('#inputComents').val();
    comment = comment.replace(/\n/g, '\t');
    //alert(comment);
    if (comment.length == 0 || comment == "Write Your comment here!" || "Comments")
    {
        //alert("Write, what You did change!");
        //return;
	comment="";
    }
    var query =id + "|" + file + "|" + from + "|" + to + "|" + text + "|" + usr +"|" + sts+"|"+comment;
    //if (query.length > 0 && query != globalQuery)
    {
        var sendQuery = query;
        //alert(globalQuery);
		console.log ('query: ', sendQuery);
        $.ajax({
            url : 'updateFeedback.php',
            dataType : 'json',
            data: {
                query : sendQuery
            }, 
            success: updateRequest
        });
    }
}

function updateFeedbackDirectly(text)
{
    var comment = "";
    var query =globalLineId+ "|" + globalFile + "|" + globalFrom + "|" + globalTo + "|" + text + "|" + user_global+"|ok|"+comment;
    //if (query.length > 0 && query != globalQuery)
    {
        var sendQuery = query;
        //alert(globalQuery);
        $.ajax({
            url : 'updateFeedback.php',
            dataType : 'json',
            data: {
                query : sendQuery
            }, 
            success: updateRequest
        });
    }
}

function getFeedback(id, file, from, to)
{
	console.log ('getFeedback (id, file, from, to): ', id, file, from, to);
    var query =id + "|" + file + "|" + from + "|" + to;
    //if (query.length > 0 && query != globalQuery)
    {
        var sendQuery = query;
		console.log ('send: ', sendQuery);
        //alert(globalQuery);
        $.ajax({
            url : 'getFeedback.php',
            dataType : 'json',
            data: {
                query : sendQuery
            }, 
            success: loadHistory
        });
    }
}
function updateRequest(data, textStatus, jqXHR)
{ 
    if (data.toString().indexOf("no_user")>=0)
		{}
    else
    {
        var tmp = data.split("|");
        getFeedback(tmp[0], tmp[1], tmp[2], tmp[3]);
    }
}

function loadHistory(data, textStatus, jqXHR)
{ 
	console.log ('Data:', data);
	console.log ('Caller: ', arguments.callee.caller);
    var inData = data.split('|'); 
    var list = Array(0);
    for (var i=1; i<inData.length-1;i++)
    if (inData[i].indexOf("ANNOTATION_VALUE") < 0) {
        list= Array(i-1);
        break;
    }
    // parse feedbackEntrys
    // Example: <ANNOTATION_VALUE sts="unapproved" usr="md" tStamp="02.07.1985 16:12:00">Это не проверенная поправка.</ANNOTATION_VALUE>
    var id = inData[0];
    for (var i=1; i < inData.length; i++)
    {
        var pt1 = inData[i].indexOf(">");
        var pt2 = inData[i].lastIndexOf("<");
        var text = inData[i].substring(pt1+1, pt2);
        var usr = "origin";
        var sts = "origin";
        var tStamp= "origin";
        if (inData[i].indexOf("sts") > 0)
        {
            var pref = inData[i].substring(0, pt1);
            var tmp = pref.split('"');
            usr = tmp[1];
            sts = tmp[3];
            tStamp = tmp[5];
        } else if (inData[i].indexOf("usr") > 0)
        {
            var pref = inData[i].substring(0, pt1);
            var tmp = pref.split('"');
            usr = tmp[1];
            tStamp = tmp[3];
        }
        if (inData[i].indexOf("COMMENT_VALUE") > 0)
        {
            for (var j=0; j < list.length; j++)
            {
                if (list[j].usr==usr && list[j].tStamp==tStamp)
                {
                    list[j]["comment"] = text;
                    break;
                }
            }
        }
        else if (inData[i].indexOf("ANNOTATION_VALUE") > 0) 
        {
            var obj = new Object();
            obj["usr"] = usr;
            obj["sts"] = sts;
            obj["tStamp"] = tStamp;
            obj["text"] = text;
            list[i-1] = obj;
        }
    }
    // create table lines
    var hystoryTable = "<tr style='color: #663300;background-color:#fb9d23'><th style='font-weight:bold'>Text</th><th style='font-weight:bold'>User</th><th style='padding:5px;font-weight:bold'>Status</th><th style='font-weight:bold'>Time</th></tr>";
    for (var i=list.length-1; i >=0 ; i--)
    if (typeof list[i]!=='undefined' && 'usr' in list[i]){
        var buf = escape($("<div/>").html(list[i]["text"]).text());
        if (i%2==0)
            hystoryTable += "<tr title='"+list[i]["comment"]+"' onClick=updateInput('"+buf+"','#dialog_"+id+"') class='dialogTableLine'><th class='dialogTableEntry'>"+list[i]["text"]+"</th><th class='dialogTableEntry'>"+list[i]["usr"]+"</th><th class='dialogTableEntry'>"+list[i]["sts"]+"</th><th class='dialogTableEntry'>"+list[i]["tStamp"]+"</th></tr>"
        else
            hystoryTable += "<tr title='"+list[i]["comment"]+"' onClick=updateInput('"+buf+"','#dialog_"+id+"') class='dialogTableLine1'><th class='dialogTableEntry'>"+list[i]["text"]+"</th><th class='dialogTableEntry'>"+list[i]["usr"]+"</th><th class='dialogTableEntry'>"+list[i]["sts"]+"</th><th class='dialogTableEntry'>"+list[i]["tStamp"]+"</th></tr>"
    }
    // load new entry to the dialog window
    //$('#tmcl-request').find('#node').html('whatever you want your html to be');
    if ($("#dialog_"+id).length>0 && $("#dialog_"+id).dialog( "isOpen" )){
	    $("#dialog_"+id).find('table').html(hystoryTable);
	    $("#dialog_"+id).find('#inputFeedback').val($("<div/>").html(list[list.length-1]["text"]).text());
    } else if (pressed){
	pressed=false;
	updateFeedbackDirectly(list[list.length-1]["text"]);
    }
    
}
function updateInput(text, id)
{
    $(id).find('#inputFeedback').val(unescape(text));
}
