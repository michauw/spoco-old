// This script will be executed exactly after load of site
user_global="";
user_status="guest";
function askAutotification(){
    $.ajax({
            url : 'checkAuthentication.php',
            dataType : 'json',
            data: {
                query : "checkAuthentication"
            }, 
            success: updateUserInformation
    });

};
function updateUserInformation(data, textStatus, jqXHR)
{ 
    if (data.toString().indexOf("no_user")>=0)
        alert("You have to log in!")
    else
    {
        var tmp = data.split("|");
        user_global = tmp[1];
        user_status = tmp[2];
        if (user_status != "admin" )
        {
           $("#adminPageButton").css("display","none");
        }
        if (user_status != "user" & user_status != "admin"  )
        {
           $("#fullTextButton").css("display","none");
        }
    }
}

$(document).ready(function() {
    askAutotification();
});

