/*var sourceLang // enthält die aktuelle source language

function setSourceLang (f) {
    for (var j=0; j<f.primlang.length; j++){
        if (f.primlang[j].checked){
            sourceLang = f.primlang[j].name
        }
    }
}

function MakeArray(n){
    for (var i=0; i<n; i++)
        this[i] = 0
    this.length = n
}

function syncPrimlang(f,t){
    var selLangs = document.getElementsByName('langs[]');
    var selLangslaenge = selLangs.length;
    for(var i=0;i<selLangslaenge;i++) { 
        if(selLangs[i].value == t){
            if(selLangs[i].checked == false){
                f.primlang[i].checked = false
            }
        }
    };
    chkForm(f)
}

function markLang(f){
    var selLangs = document.getElementsByName('langs[]');
    var selLangslaenge = selLangs.length;
    for(var i=0;i<selLangslaenge;i++) { 
        if(f.primlang[i].checked == true){
            selLangs[i].checked = true
        }
    };
    chkForm(f)
}

function chkForm (f) {
    var ok1 = false
    var ok2 = false
    var selLangs = document.getElementsByName('langs[]');
    var selLangslaenge = selLangs.length;
    for (var j=0; j<f.primlang.length; j++){
        if (f.primlang[j].checked){
            ok1 = true
        }
    }
    for(var i=0;i<selLangslaenge;i++) { 
        if((selLangs[i].checked)&&(!f.primlang[i].checked)){
            ok2 = true
        }
    }	
    //	if(ok1 && ok2){ document.forms["languages"].submit() } else {
    if(ok1 && ok2){
        return true
    } else {
        alert('Please select a source language and at least one target language!')
        f.focus()
    }
}

function chkAll(t) {
    var seltexts = document.getElementsByName(t);
    var seltextslaenge = seltexts.length;
    for(var i=0;i<seltextslaenge;i++) { 
        seltexts[i].checked = true
    }
}

function chkNone(t) {
    var seltexts = document.getElementsByName(t);
    var seltextslaenge = seltexts.length;
    for(var i=0;i<seltextslaenge;i++) { 
        seltexts[i].checked = false
    }
}

function Go (select) {
    var wert = select.options[select.options.selectedIndex].value;
    if (wert == "leer") {
        select.form.reset();
        parent.frames["kwic"].focus();
        return;
    } else {
        parent.frames["kwic"].location.href = wert;
        select.form.reset();
        parent.frames["kwic"].focus();
    }
}*/

// Extract query from CQPBox and save it to the outQuery (outQuery will be send to the server
function qcpQuery() {
    document.getElementById("outQueryCQP").value=document.getElementById("qcp").value;
}
// Remove spaces at the ends of string
function removeSpaces(str)
{
    var bufStr;
    var begin = 0;
    var end = str.length-1;
    while (str[begin] == " ") begin++;
    while (str[end] == " ") end--;
    return str.substring(begin, end+1);	
}

function replaceAndEscapeChrs(value){
    value = value.replace(/\(/g, "\\(");
    value = value.replace(/\)/g, "\\)");
    value = value.replace(/\[/g, "\\[");
    value = value.replace(/\]/g, "\\]");
    value = value.replace(/\{/g, "\\}");
    value = value.replace(/\{/g, "\\}");
    value = value.replace(/\+/g, "\\+");
    value = value.replace(/\_/g, "\\_");
    value = value.replace(/\-/g, "\\-");
    value = value.replace(/\*/g, "\\*");
    value = value.replace(/\./g, "\\.");
    value = value.replace(/\:/g, "\\:");
    value = value.replace(/\;/g, "\\;");
    value = value.replace(/\,/g, "\\,");
    value = value.replace(/\?/g, "\\?");
    value = value.replace(/\!/g, "\\!");
    return value;
}
// Extract query from ExactQueryBox and save it to the outQuery (outQuery will be sent to the server)
function exactPhraseQuery() 
{
    var quellText = removeSpaces(document.getElementById("exactQ").value);
    document.getElementById("exactQ").value = quellText;
    quellText = replaceAndEscapeChrs(quellText);
    var resultText = "\"";
    var i = 0;
    // make from 'hallo you' query : '"hallo" "you"'
    while (i < quellText.length)
    {
        var ch = quellText.charAt(i);
        if (ch == ' ')
        {
            resultText+="\"";
        }
        resultText+=ch;
        if (ch == ' ')
        {
            resultText+="\"";
        }
        i++;
    }
    resultText+="\"";
    // extract case-sensitive checkbox from exactphrasesearch-box
    var checkBoxes = document.getElementsByName("checkBox0");
    var caseSensitive = checkBoxes[0].checked;
    // check if 'case sensitive' seted
    if (!caseSensitive)
        resultText= resultText + "%c";
    document.getElementById("outQueryExact").value=resultText;
//alert(document.getElementById("outQueryExact").value);
}

// Extract query from AdvancedSearchBox and save it to the outQuery (outQuery will be sent to the server)
function boundQuery(){
    var begin = document.getElementById("bQBegin").value.replace(/ /g, "");
    var beginL = document.getElementById("bQBeginL").value.replace(/ /g, "");
    var beginT = document.getElementById("bQBeginT").value.replace(/ /g, "");
    document.getElementById("bQBegin").value = begin;
    //removed escaping
    //begin = replaceAndEscapeChrs(begin);
    var checkBoxes = document.getElementsByName("checkBox1");

    //alert ("HH");	
    // get word attribute
    if (document.getElementById("bQBegin").value!=""){
    // extract case-sensitive checkbox from advancedsearch-box
    var caseSensitive = checkBoxes[2].checked;
    if (checkBoxes[0].checked)
        begin = begin + ".*";
    if (checkBoxes[1].checked)
        begin = ".*" + begin;

    begin = "word=\"" + begin + "\"";
    
    // check if 'case sensitive' set
    if (!checkBoxes[2].checked)
        begin = begin + "%c";

    }

    // get lemma attribute
    if (document.getElementById("bQBeginL").value!=""){
    // extract case-sensitive checkbox from advancedsearch-box
    if (begin != "") begin=begin+" & ";
    begin = begin + " lemma=\"" + beginL + "\"";
    }

    // get tag attribute
    if (document.getElementById("bQBeginT").value!=""){
    // extract case-sensitive checkbox from advancedsearch-box
    if (begin != "") begin=begin+" & ";
    begin = begin + " tag=\"" + beginT + "\"";
    }



    var query = "[" + begin + "]";

    // Read queries from all boxes of AdvancedSearchBox
    for (var i=2; i <= boxN; i++)
        if ((document.getElementById("bQEnd" + i).value!="")|(document.getElementById("bQEndL" + i).value!="")|(document.getElementById("bQEndT" + i).value!=""))
        {
				
            var from = 0; 
            var to = 0;
            var fromId = "bQFrom" + i;
            var toId = "bQTo" + i;
            var endId = "bQEnd" + i;
            var endIdL = "bQEndL" + i;
            var endIdT = "bQEndT" + i;
            var checkBoxId = "checkBox" + i;
            var end = document.getElementById(endId).value.replace(/ /g, "");
            document.getElementById(endId).value = end;
   			    //removed escaping
            //end = replaceAndEscapeChrs(end);
            checkBoxes = document.getElementsByName(checkBoxId);


			//get word attribute
				if (document.getElementById(endId).value!=""){

            if (checkBoxes[0].checked)
                end = end + ".*";
            if (checkBoxes[1].checked)
                end = ".*" + end ;

	    end = "word=\"" + end + "\"";

	    // check if 'case sensitive' set
	    if (!checkBoxes[2].checked)   end = end + "%c";
  	}
    //alert("ddd");
		
    // get lemma attribute
    if (document.getElementById(endIdL).value!=""){
	    // extract case-sensitive checkbox from advancedsearch-box
	    if (end != "") end =end+" & ";
	    end = end + " lemma=\"" + document.getElementById(endIdL).value + "\"";
    }

    // get tag attribute
    if (document.getElementById(endIdT).value!=""){
    // extract case-sensitive checkbox from advancedsearch-box
    if (end != "") end=end+" & ";
    end = end + " tag=\"" + document.getElementById(endIdT).value + "\"";
    }





            if ((document.getElementById(fromId).value!="")&&(document.getElementById(toId).value!=""))
            {
                from = document.getElementById(fromId).value;
                to = document.getElementById(toId).value;
            } else if ((document.getElementById(fromId).value=="")&&(document.getElementById(toId).value==""))
{
                document.getElementById(fromId).value=1;
                document.getElementById(toId).value=1;
                from = document.getElementById(fromId).value;
                to = document.getElementById(toId).value;
            } else if ((document.getElementById(fromId).value!="")&&(document.getElementById(toId).value==""))
{
                document.getElementById(toId).value=document.getElementById(fromId).value;
                from = document.getElementById(fromId).value;
                to = document.getElementById(toId).value;
            } else if ((document.getElementById(fromId).value=="")&&(document.getElementById(toId).value!=""))
{
                document.getElementById(fromId).value=document.getElementById(toId).value;
                from = document.getElementById(fromId).value;
                to = document.getElementById(toId).value;
            }
            query+="[]{"+from+","+to+"}" + "[" + end + "]";

        }

    document.getElementById("outQueryAdvanced").value=query;
(document.getElementById("outQueryAdvanced").value);
}

// This function makes Advancedserach visible/unvisible by switching plus-image
function extendedSearch(){
    if (document.getElementById("box2").style.visibility!="visible")
    {
        for (var i=2; i <= boxN; i++)
        {
            var bf = "box" + i;
            document.getElementById(bf).style.visibility="visible";
        }
        document.getElementById("bigBox2").style.visibility="visible";
        document.getElementById("boundBtn").style.visibility="visible";
        document.getElementById("boundBtnXml").style.visibility="visible";
        document.getElementById("info2").style.visibility="visible";
        document.getElementById("info3").style.visibility="visible";
        document.getElementById("boxQCP").style.visibility="visible";
        document.getElementById("bigBoxQCP").style.visibility="visible";
        document.getElementById("qcpBtn").style.visibility="visible";
        document.getElementById("qcpBtnXml").style.visibility="visible";
        document.getElementById("up").style.visibility="visible";
        document.getElementById("down").style.visibility="visible";
        document.getElementById("l1").innerHTML =  "Exact Phrase:";
    }
    else
    {
        for (var i=2; i <= boxN; i++)
        {
            var bf = "box" + i;
            document.getElementById(bf).style.visibility="collapse";
        }
        document.getElementById("bigBox2").style.visibility="collapse";
        document.getElementById("boundBtn").style.visibility="collapse";
        document.getElementById("boundBtnXml").style.visibility="collapse";
        document.getElementById("info2").style.visibility="collapse";
        document.getElementById("info3").style.visibility="collapse";
        document.getElementById("boxQCP").style.visibility="collapse";
        document.getElementById("bigBoxQCP").style.visibility="collapse";
        document.getElementById("qcpBtn").style.visibility="collapse";
        document.getElementById("qcpBtnXml").style.visibility="collapse";
        document.getElementById("up").style.visibility="collapse";
        document.getElementById("down").style.visibility="collapse";
        document.getElementById("l1").innerHTML =  "";
    }
}

// Add a new Word and bound-inputs to advancedsearch
function openBox()
{
    boxN++;
    var id = boxN;
    var boxId = "box" + id;
    var parentBoxId = "#box" + (id - 1);
    var textId = "bQEnd" + id;
    var htmlString =       '<br id="br' + id + '"/>'
    + '<div id="box' + id + '" style="width:100%; float: left; visibility:visible">'
    + '<div  class="gradient-opacity"; style="float:left; width:830px; height:34px;">'
    + '	<label style="margin-left:530px; width:140px; font-weight: bold; font-family:\'EB Garamond\', serif; font-size:15px;">Tokens in between:</label>'
    + '	<label style="font-weight: lighter; font-family:\'EB Garamond\', serif; font-size:12px; width:35px; margin-left:10px;" > from </label>'
    + '	<input type="text" id="bQFrom' + id + '" style="font-size:12px; font-family:Garamond, serif; width:30px" name="bound_begin" value="0"/>'
    + '	<label style="font-weight: lighter; font-family:\'EB Garamond\', serif; font-size:12px; width:30px; text-align:center">to</label>'
    + '	<input type="text" id="bQTo' + id + '" style="font-size:12px; font-family:Garamond, serif; width:30px" name="bound_end" value="0"/>'
    + '</div>'
    + '<div  class="gradient-opacity2"; style="float:left; width:30px; height:34px;"></div>'
    + '<br />'
    + '<br />'    
    + '	<label style="float-left;  font-weight: bold; font-family:\'EB Garamond\', serif; font-size:15px; margin-left:10px; width:auto;"> ' + id + '. Token</label>'
    + '	<input style="width:210px; margin-left:5px; float:left"; type="text" id="bQEnd' + id + '" onkeyup=getSuggestion("bQEnd' + id + '","word") name="query_end" class="keyboardInput"/>'
    + '	<label style="float:left; font-weight: bold; font-family:\'EB Garamond\', serif; font-size:15px; margin-left:10px; width:auto;"> Lexeme: </label>'
    + '	<input style="width:210px; margin-left:5px; float:left"; type="text" id="bQEndL' + id + '" onkeyup=getSuggestion("bQEndL' + id + '","lemma") name="query_endL" class="keyboardInput"/>'
    + '	<label style="float:left; font-weight: bold; font-family:\'EB Garamond\', serif; font-size:15px; margin-left:10px; width:auto;"> Gram. tag: </label>'
    + '	<input style="width:115px; margin-left:5px; float:left"; type="text" id="bQEndT' + id + '" onkeyup=getSuggestion("bQEndT' + id + '", "tag") name="query_endT" class="keyboardInput"/>'
    + '<div style="float: right; margin-right:30px;">'
    + '<select name="pos_select'+ id + '" id="pos_select'+ id + '" onchange="populateField'+id+'();">'
    + posForm
    + '</select></div>'
    + '<script>'
    + 'function populateField'+id+'() {'
    + 'postag = document.getElementById("pos_select'+id+'").value;'
    + 'document.getElementById("bQEndT' + id + '").value = postag;'
    + 'if (postag && postag !="C.*" && postag != "Q.*" && postag != "I.*") {'
    + 'var posField = "bQEndT' + id + '";'
    + 'console.log(posField);'
    + 'posPopUp(postag,posField);'
    + 'delete postag;'
    + '}'
    + '}'
    + '</script>'
    + '<br />'
    + '	<input type="checkbox" style="margin-left:10px; width:10px;" name="checkBox' + id + '" value="yes1"/>'
    + '	<label style="font-weight: lighter; font-family:\'EB Garamond\', serif; font-size:12px; margin-left:10px; width:70px;"> begins with</label>'
    + '	<input type="checkbox" style=" width:10px;" name="checkBox' + id + '" value="yes2"/>'
    + '	<label style="font-weight: lighter; font-family:\'EB Garamond\', serif; font-size:12px; margin-left:10px; width:65px;"> ends with</label>'
    + '	<input type="checkbox" style=" width:10px;" name="checkBox' + id + '" value="yes3"/>'
    + '	<label style="font-weight: lighter; font-family:\'EB Garamond\', serif; font-size:12px; margin-left:10px; width:65px;"> case sensitive</label>'
    + '</div>';
    $(parentBoxId).after(htmlString);
	keyboard ()	
}

// Remove last word from advanced search
function closeBox(){
    var boxId = "#box" + boxN;
    var brId = "#br" + boxN;
    if (boxN  > 2)
    {
        $(boxId).remove();
        $(brId).remove();
        boxN--;
    }
}

// Autocomplete query will be saved here
var globalQuery="xx";


// Extract query from input-field (or other given string)
// Example: string str "make it" functions return query-string '"m.*"[]{0,0}"i.*"'
function extractQuery(str, at_type)
{
    var query = "";
    var i = 0;
    //alert (str)
    // returns empty string if str is empty or begins with space or tab 
    if (str.length < 1 || str[i]==" " || str[i]=="\t") return ""; 
    // extract query character or the first word 
    query = '[' +at_type+ '="' + str[i] + '.*"] ';
    i++;
    while(i < str.length)
    {
        if (str[i]==" ")
        {
            i++;
            if (i >= str.length || str[i]==" " || str[i]=="\t") return query;
     					query = ' [' +at_type+ '="' + str[i] + '.*"] ';
        }
        i++;
    }
    return query;	 
}

// Ajax-connector between autocomplete.php and UserInterface
function getSuggestion(id, at_type)
{
    //alert(id)
    //alert (at_type)
    var query = extractQuery(document.getElementById(id).value, at_type);
    if (query.length > 0 && query !== globalQuery)
    {
        globalQuery = query;
        //alert(globalQuery);
        $.ajax({
            url : 'autocomplete.php',
            dataType : 'json',
            data: {
                query : globalQuery,
                attribute : at_type
            }, 
            success: loadData
        });
    }
}

// List of suggestions/words/phrases, wich was sent from server
var dataAutocomplete;

// Load words and phrases, wich we have got from server (autocomplete.php) to jquery-autocomplete
function loadData(data, textStatus, jqXHR)
{ 
    dataAutocomplete = data;
	//alert(data);
    $( ".keyboardInput" ).autocomplete({
        source: dataAutocomplete,
		select: function(event, ui) {
			if (angular.element($(this)).scope().atype == 'word')
				angular.element($(this)).scope().queryRow.token = ui.item.value;
			else
				angular.element($(this)).scope().queryRow.lexeme = ui.item.value;
			angular.element($(this)).scope().$apply();
		}
    });
    $( ".keyboardInput" ).autocomplete( "option", "minLength", 2);
    $( ".keyboardInput" ).autocomplete( "option", "delay", 0);

}
