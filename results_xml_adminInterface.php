<?php
include('include/init.php'); 
// retrieve defaults
$CQPOPTIONS = " ";
if ($CQPINIT) {
    $CQPOPTIONS .= " -I $CQPINIT";
};
if ($HARDBOUNDARY) {
    $CQPOPTIONS .= " -b $HARDBOUNDARY";
}

if (isset($_POST['btn']['searchNew'])) {
$query = $_POST["query"];
} 

if (isset($_POST['btn']['allNew'])) {
$query = ' <utterance_sts="new|unconfirmed|unapproved">[] expand to utterance';
} 



$pstructures = 'set ShowTagAttributes on; set PrintStructures "utterance, utterance_from, utterance_to, utterance_file, utterance_spkr, utterance_fromId, utterance_toId, utterance_usr, utterance_sts, utterance_date"; ';
$showtags = ' show +tag; show +lemma; ';
$execstring = "$CWBDIR" . "cqpcl -r $REGISTRY" . " '".$CORPUSNAME ."; set Context 1 utterance; set PrintMode sgml;  " . $showtags . $pstructures . $query . ";'";
$execstring .= " | sed -r 's/&lt;utterance_from ([^&]*?)&gt;/<from>\\1<\/from>/g'";
$execstring .= " | sed -r 's/&lt;utterance_to ([^&]*?)&gt;/<to>\\1<\/to>/g'";
$execstring .= " | sed -r 's/&lt;utterance_file ([^&]*?)&gt;/<file>\\1<\/file>/g'";
$execstring .= " | sed -r 's/&lt;utterance_spkr ([^&]*?)&gt;/<spkr>\\1<\/spkr>/g'";
$execstring .= " | sed -r 's/&lt;utterance_fromId ([^&]*?)&gt;/<fromId>\\1<\/fromId>/g'";
$execstring .= " | sed -r 's/&lt;utterance_toId ([^&]*?)&gt;/<toId>\\1<\/toId>/g'";
$execstring .= " | sed -r 's/&lt;utterance_sts ([^&]*?)&gt;/<sts>\\1<\/sts>/g'";
$execstring .= " | sed -r 's/&lt;utterance_usr ([^&]*?)&gt;/<usr>\\1<\/usr>/g'";
$execstring .= " | sed -r 's/&lt;utterance_date ([^&]*?)&gt;/<date>\\1<\/date>/g'";
$execstring .= " | sed -r 's/&lt;(\/)?utterance&gt;/<utterance\/>/g'";
$execstring .= " | sed -r 's/&lt;(\/)?utterance&gt;/<utterance\/>/g'";
$execstring .= " | sed -r 's/\/__UNDEF__//g'";
$execstring .= " | sed -r 's/(<TOKEN>[^\/<]+)\/([^>]+)<\/TOKEN>/\\1<ANNOT>\\2<\/ANNOT><\/TOKEN>/g'";
$execstring .= " | sed -r 's/<attribute[^>]+>//g'";
$execstring .= "\n";

unset($out);
exec($execstring, $out);

$outstr = trim(implode("\n", $out));
$outstr = '<RESULTS>' . $outstr . '</RESULTS>';

if (isset($_POST['btn']['allNew'])) {
    header('Content-type: text/xml; charset=utf-8');
    echo('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="./revise-changes.xsl" ?>');
    echo ($outstr);
};

if (isset($_POST['btn']['searchNew'])) {
    header('Content-type: text/xml; charset=utf-8');
    echo('<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="./revise-changes.xsl" ?>');
    echo ($outstr);
};
if (isset($_POST['btn']['reEncodeCorpus'])) {
    echo('<h1>The corpus is being reencoded NOW.</h1>');
    echo('All the ELAN-Files are being processed and converted to the online corpus. ');
    echo('After this, all the changes that were approved manually will appear as approved in the corpus. ');
    echo('This is done automatically each night anyway. ');
    echo('<h1>WAIT.</h1>');
    echo('<h1>BE PATIENT.</h1>');
    echo('<h1>HAVE SOME TEA.</h1>');
    echo('<body>HAVE SOME TEA.</body>');
    echo('    <pre>');
    system("sh runThis.sh");
    echo('    </pre>');
	
};
?>
